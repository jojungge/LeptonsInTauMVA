FROM atlas/athanalysis:21.2.137
ADD . /jojungge/LeptonsInTauMVA
WORKDIR /jojungge/build
RUN source ../LeptonsInTauMVA/HDF5Writing/test/setup_ci_release.sh &&  sudo chown -R atlas /jojungge && cmake ../LeptonsInTauMVA &&  make 
