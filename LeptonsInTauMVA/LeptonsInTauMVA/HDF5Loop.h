#ifndef LeptonsInTauMVA_HDF5LOOP_H
#define LeptonsInTauMVA_HDF5LOOP_H
///
#include <HDF5Writing/HDF5ClassFactory.h>
#include <LeptonsInTauMVA/EventCounter.h>
#include <LeptonsInTauMVA/MuonAnalysis.h>
#include <LeptonsInTauMVA/SampleGroup.h>
#include <LeptonsInTauMVA/Utils.h>
namespace LeptonTauMVA {

    class HDF5Loop {
    public:
        HDF5Loop(std::shared_ptr<XAMPP::HDF5ClassFactory> factory, std::shared_ptr<SampleGroupMgr> train_sample,
                 std::shared_ptr<SampleGroupMgr> test_sample, std::shared_ptr<EventCounter> ev_counter);

    public:
        Long64_t process_file(const std::string& file_name, std::function<std::vector<double>(MuonAnalysis&)> var_filler);
        Long64_t process_file(std::shared_ptr<TFile> r_file, std::function<std::vector<double>(MuonAnalysis&)> var_filler);

        Long64_t start_threads(std::function<std::vector<double>(MuonAnalysis&)> var_filler);

    private:
        Long64_t process_file(std::shared_ptr<TFile> r_file, const std::string& tree_name,
                              std::function<std::vector<double>(MuonAnalysis&)> var_filler);

        std::shared_ptr<XAMPP::HDF5ClassFactory> m_factory;
        std::shared_ptr<SampleGroupMgr> m_train_sample;
        std::shared_ptr<SampleGroupMgr> m_test_sample;
        std::shared_ptr<EventCounter> m_ev_counter;
    };
}  // namespace LeptonTauMVA
#endif
