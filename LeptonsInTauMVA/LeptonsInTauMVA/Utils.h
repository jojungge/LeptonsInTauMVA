#ifndef LeptonsInTauMVA_UTILS_H
#define LeptonsInTauMVA_UTILS_H
///
#include <HDF5Writing/Utils.h>
#include <NtupleAnalysisUtils/AtlasStyle.h>
#include <NtupleAnalysisUtils/CanvasOptions.h>
#include <NtupleAnalysisUtils/DefaultPlotting.h>
#include <NtupleAnalysisUtils/NtupleBranch.h>
#include <NtupleAnalysisUtils/Plot.h>
#include <NtupleAnalysisUtils/PlotContent.h>
#include <NtupleAnalysisUtils/PlotUtils.h>
#include <TFile.h>
#include <TH1.h>
#include <TStopwatch.h>
#include <TTree.h>

#include <iostream>
#include <vector>

// Defined in PlotUtils
//      --   std::string RandomString(size_t length);
//      --   TDirectory* mkdir(const std::string &name, TDirectory *in_dir);
//      --   std::shared_ptr<TFile> Open(const std::string &Path);
//      --   std::string ResolveEnviromentVariables(std::string str);
//      --   std::string ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep);
// Defined in namespace XAMPP
//      --   bool DoesDirectoryExist(const std::string& Path);
//      --   std::string WhiteSpaces(int N = 0, std::string space = " ");

std::ostream &operator<<(std::ostream &os, TStopwatch &cpu_timer);
namespace LeptonTauMVA {

    static constexpr float MeVtoGeV = 1.e-3;
    /// Simple copy of the enums defined in this file
    /// https://acode-browser.usatlas.bnl.gov/lxr/source/athena/Tracking/TrkEvent/TrkEventPrimitives/TrkEventPrimitives/ParamDefs.h
    ///
    enum PerigeeParams {
        d0 = 0,
        z0 = 1,
        phi0 = 2,
        theta = 3,
        qOverP = 4,
        num_perigess = 5,
    };
    std::string to_string(PerigeeParams param);
    std::string to_string(PerigeeParams param1, PerigeeParams param2);

    /// Convert a given time in seconds into HH::MM::SS
    std::string TimeHMS(float t);

    std::vector<std::string> get_good_files(const std::vector<std::string> &in_files, const std::string &tree_name, Long64_t &tot_events,
                                            bool skip_empty = true);
    std::vector<std::string> get_good_files(const std::vector<std::string> &in_files, const std::string &tree_name, bool skip_empty = true);

    /// Erases all trailing and beginning white spaced from the string
    std::string EraseWhiteSpaces(std::string str);
    /// Returns the longest string shared between the two input strings
    std::string LongestCommonString(const std::string &str1, const std::string &str2);
    /// Lists a direcotry where each element needs to contain the wildcard
    std::vector<std::string> ListDirectory(std::string Path, const std::string &wild_card = "");

    std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &Files);
    /// Returns true if a new line could be piped from an ifstream instance
    bool GetLine(std::ifstream &inf, std::string &line);

    std::vector<std::string> ReadList(const std::string &list_path);
    /// Pulls over and underflows into the last and first bin
    void pull_overflow(std::shared_ptr<TH1> H);

    std::shared_ptr<TH1> clone(const std::shared_ptr<TH1> &histo, bool reset = false);
    std::shared_ptr<TH1> clone(const TH1 *histo, bool reset = false);

    std::shared_ptr<TH1> MakeTH1(std::vector<double> BinEdgesX, std::vector<double> BinEdgesY = std::vector<double>(),
                                 std::vector<double> BinEdgesZ = std::vector<double>());
    /// Make a vector containing the numbers ranging between start and end, including the boundaries
    std::vector<int> make_range(int start, int end);
    /// Returns all bins of the histogram
    unsigned int GetNbins(const std::shared_ptr<TH1> &Histo);
    unsigned int GetNbins(const TH1 *Histo);
    /// Returns the central bins of the histogram
    unsigned int GetNVisBins(const std::shared_ptr<TH1> &Histo);
    unsigned int GetNVisBins(const TH1 *Histo);
    /// Returns the number of overlfow bins
    unsigned int GetNOverFlowBins(const std::shared_ptr<TH1> &Histo);
    unsigned int GetNOverFlowBins(const TH1 *Histo);

    bool isOverflowBin(const std::shared_ptr<TH1> &Histo, int bin);
    bool isOverflowBin(const TH1 *Histo, int bin);
    bool isOverflowBin(const TAxis *a, int bin);

    template <typename T> void Remove(std::vector<T> &vec, std::function<bool(const T &)> func) {
        typename std::vector<T>::iterator itr = std::find_if(vec.begin(), vec.end(), func);
        while (itr != vec.end()) {
            vec.erase(itr);
            itr = std::find_if(vec.begin(), vec.end(), func);
        }
    }
    template <typename T> void Remove(std::vector<T> &Vec, const T &Ele) {
        Remove<T>(Vec, [&Ele](const T &in_vec) { return Ele == in_vec; });
    }
    template <typename T>
    void Copy(const std::vector<T> &From, std::vector<T> &To, std::function<bool(const T &)> func, bool Clear = false) {
        if (Clear) To.clear();
        To.reserve(From.size() + To.capacity());
        for (auto &Ele : From) {
            if (func(Ele)) To.push_back(Ele);
        }
        To.shrink_to_fit();
    }
    template <typename T> void Copy(const std::vector<T> &From, std::vector<T> &To, bool Clear = false) {
        Copy<T>(
            From, To, [&To](const T &ele) { return !XAMPP::IsElementInList(To, ele); }, Clear);
    }
    template <typename T> void ClearFromDuplicates(std::vector<T> &toClear) {
        std::vector<T> copy = toClear;
        Copy(copy, toClear, true);
    }
    template <typename T>
    unsigned int count(typename std::vector<T>::const_iterator begin, typename std::vector<T>::const_iterator end,
                       std::function<bool(const T &)> func) {
        unsigned int n = 0;
        for (; begin != end; ++begin) {
            if (func(*begin)) ++n;
        }
        return n;
    }

    template <typename T> unsigned int count(const std::vector<T> &vector, std::function<bool(const T &)> func) {
        return count<T>(vector.begin(), vector.end(), func);
    }
    template <typename T> unsigned int count_active(const std::vector<std::future<T>> &threads) {
        return count<std::future<T>>(threads, [](const std::future<T> &th) {
            using namespace std::chrono_literals;
            return th.wait_for(0ms) != std::future_status::ready;
        });
    }
}  // namespace LeptonTauMVA
#endif
