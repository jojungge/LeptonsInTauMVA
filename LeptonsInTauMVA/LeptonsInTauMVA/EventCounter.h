#ifndef LeptonsInTauMVA_EVENTCOUNTER_H
#define LeptonsInTauMVA_EVENTCOUNTER_H
///
#include <LeptonsInTauMVA/Utils.h>

namespace LeptonTauMVA {
    class EventCounter {
    public:
        EventCounter(const std::vector<std::string>& in_files, const std::string& tree_name);

        std::string next();
        std::shared_ptr<TFile> open_next();
        void update_counter(Long64_t proc_events);
        void finish_file(Long64_t ev_in_file, Long64_t proc_events, Long64_t prompt_interval);

        std::string tree() const;
        size_t num_files() const;
        Long64_t tot_events() const;
        Long64_t proc_events() const;

    private:
        std::vector<std::string> m_in_files;
        std::string m_tree_name;
        size_t m_file_counter;
        size_t m_files_finished;
        std::mutex m_next_mutex;
        std::mutex m_proc_ev_mutex;
        Long64_t m_tot_ev;
        Long64_t m_prompt_ev;
        Long64_t m_proc_ev;
        TStopwatch m_watch;
        bool m_watch_started;
    };
}  // namespace LeptonTauMVA
#endif
