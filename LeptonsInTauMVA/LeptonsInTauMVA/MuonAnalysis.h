#ifndef LeptonTauMVA__MUONANALYSIS__H
#define LeptonTauMVA__MUONANALYSIS__H

#include <LeptonsInTauMVA/MuonTree.h>
#include <LeptonsInTauMVA/MuonTree_v5.h>
#include <NtupleAnalysisUtils/AtlasStyle.h>
#include <NtupleAnalysisUtils/DefaultPlotting.h>
#include <NtupleAnalysisUtils/HistoFiller.h>
#include <NtupleAnalysisUtils/NtupleBranch.h>
#include <NtupleAnalysisUtils/Plot.h>
#include <NtupleAnalysisUtils/PlotUtils.h>
#include <NtupleAnalysisUtils/Sample.h>
#include <NtupleAnalysisUtils/Selection.h>
#include <TTree.h>

#include <vector>

///     The covariance matrix of a track refers to the
///     uncertainties of the associated pergiees. In the
///     n-tuples the matrix is stored as a 15-dim array
///     obtained from the TrackParticle->definingParametersCovMatrixVec() const method
///     The mapping between the 1D vector and the 2D symmetrical matrix is as follows
///         [0] --> (0,0)
///         [1] --> (1,0) or (0,1)
///         [2] --> (1,1)
///         [3] --> (2,0) or (0,2)
///         [4] --> (2,1) or (1,2)
///         [5] --> (2,2)
///         [6] --> (3,0) or (0,3)
///         [7] --> (3,1) or (1,3)
///         [8] --> (3,2) or (2,3)
///         [9] --> (3,3)
///        [10] --> (4,0) or (0,4)
///        [11] --> (4,1) or (1,4)
///        [12] --> (4,2) or (2,4)
///        [13] --> (4,3) or (3,4)
///        [14] --> (4,4) or (4,4)
namespace LeptonTauMVA {
    class MuonAnalysis;
    typedef PlotFillInstructionWithRef<TH1D, MuonAnalysis> PlotFiller1D;
    typedef PlotFillInstructionWithRef<TH2D, MuonAnalysis> PlotFiller2D;

    typedef Selection<MuonAnalysis> Cut;

    class MuonAnalysis : public MuonTree_v5 {
    public:
        MuonAnalysis(TTree* t);
        /// Returns whether the muon is from a direct
        /// EWK gauge boson decay without
        /// intermediate tau lepton
        bool is_prompt_muon();
        /// Returns whether the muons is from a prompt
        /// EWK gauge boson decay which decayed intermediately
        /// into a tau lepton
        bool is_from_prompt_tau();
        /// Returns whether the muon originates from
        /// a background process
        bool is_background_muon();
        /// Classifies the muon according to the enum scheme defined in
        /// SampleGroup.h -- b_Background,  b_PromptMuon,
        ///                  b_PromptMuonFromTau,
        int classify_muon();
        ///	Returns the rho prime variable defined as
        ///  pt_{ID} - pT_{MS} / pt_{CB}
        ///  using the calibrated muon momenta
        float rho_prime();
        ///	Returns the  q_over_p significance
        ///  defined as
        ///		(q/P)_{ID} - (q/P)_{MS} / hypot ( sigma(q/P)_{ID} , sigma(q/P)_{MS})
        float qover_p_significance();
        ///	Returns the track isolation of the muon defined  as
        ///		ptvarcone30_TightTTVA_pt1000 / pt
        ///
        float track_iso();
        ///	Returns the calorimeter isolation of the muon defined as
        ///		topoetcone20 / pt
        float calo_iso();
        ///	Returns the distance between the muons associated vertex
        ///	to the primary vertex
        float muon_vtx_z0();

        /// Returns the sin theta of the muon
        float sin_theta();
        /// Returns the d0 significane of the combined track
        float d0_significance();
        float z0_significance();
        float rel_pt_err();

        /// Returns the chi2 / nDoF of the variuos tracks
        float ID_chi2OverNDoF();
        float MS_chi2OverNDoF();
        float CB_chi2OverNDoF();

        /// returns the covariance matrix entries of the various tracks
        float ID_cov(int i, int j);
        float CB_cov(int i, int j);
        float MS_cov(int i, int j);
        /// returns the perigee parameters of the various tracks
        float ID_param(int i);
        float CB_param(int i);
        float MS_param(int i);

        std::vector<double> ID_cov_vec();
        std::vector<double> CB_cov_vec();
        std::vector<double> MS_cov_vec();

        /// returns the sqrt of the covariance matirces
        float sqrt_ID_cov(int i, int j);
        float sqrt_CB_cov(int i, int j);
        float sqrt_MS_cov(int i, int j);
        std::vector<double> sqrt_ID_cov_vec();
        std::vector<double> sqrt_CB_cov_vec();
        std::vector<double> sqrt_MS_cov_vec();
        /// returns param_[1]*param[j] / cov(i,j)
        /// --> in caces of 0 cov(i,j) the number is divides by infty
        float ID_perigee_sig(int i, int j);
        float MS_perigee_sig(int i, int j);
        float CB_perigee_sig(int i, int j);

        std::vector<double> ID_perigee_sig_vec();
        std::vector<double> MS_perigee_sig_vec();
        std::vector<double> CB_perigee_sig_vec();

    private:
        std::vector<double> cov_to_vec(std::function<float(int i, int j)>, const size_t s = 15);
        int cov_idx(int i, int j) const;
        float sign_sqrt(const float x) const;
    };
}  // namespace LeptonTauMVA
#endif
