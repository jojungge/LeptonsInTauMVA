#ifndef LeptonsInTauMVA_SAMPLEGROUP_H
#define LeptonsInTauMVA_SAMPLEGROUP_H
///
#include <LeptonsInTauMVA/MuonAnalysis.h>
#include <LeptonsInTauMVA/Utils.h>
namespace LeptonTauMVA {
    class SampleGroup {
    public:
        enum EventType {
            b_Background = 0,
            b_PromptMuon = 1,
            b_PromptMuonFromTau = 2,

            Background = 1 << b_Background,
            PromptMuon = 1 << b_PromptMuon,
            PromptMuonFromTau = 1 << b_PromptMuonFromTau,
            All = Background | PromptMuon | PromptMuonFromTau
        };

        SampleGroup(const std::vector<int>& dsids, unsigned int limit_events, unsigned int ev_types = EventType::All);
        void add_sample(int dsid);
        bool in_group(int dsid) const;
        int type_mask() const;
        virtual bool add_event(MuonAnalysis& t);

        bool limit_reached() const;
        const std::vector<int>& dsids() const;
        unsigned int events_filled() const;
        unsigned int maximum() const;

    protected:
        bool increment_counter();

    private:
        std::vector<int> m_dsids;

        unsigned int m_ev_max;
        unsigned int m_ev_cached;
        unsigned int m_types;
        std::mutex m_mutex_add_event;
        std::mutex m_mutex_increment;
    };
    /// The importances of some features might stronlgy depend
    /// on the actual kinematic region of the muon. In fact,
    /// it's well known that background muons tend to have lower momenta
    /// than muons from Z or W decays and the muons from tau decays
    /// are in between.
    /// Having this fact in mind, one realizes that the muons from tau
    /// decays might be easily discarded as prompt muons by a trained
    /// classifier, which fully misdirects the aim of this project as
    /// a classifier should be designed deciding on the extra variables
    /// whether the muon at a given momentum and pt is actually signal or
    /// not.
    /// The FlatShapeGroup sample divides the maximum allowed statistcs
    /// in n disjoint phase space regions
    class FlatlShapeGroup : public SampleGroup {
    public:
        FlatlShapeGroup(const std::vector<int>& dsids, unsigned int limit_events, std::shared_ptr<TH1> histo_shape, unsigned int ev_types,
                        std::function<int(MuonAnalysis& t, TH1*)> find_bin);

        static std::shared_ptr<FlatlShapeGroup> make_shared(
            const std::vector<int>& dsids, unsigned int limit_events, std::shared_ptr<TH1> histo_shape,
            unsigned int ev_types = EventType::All, std::function<int(MuonAnalysis& t, TH1*)> find_bin = [](MuonAnalysis& t, TH1* h) {
                return h->GetXaxis()->FindBin(t.Muons_eta());
            }) {
            return std::make_shared<FlatlShapeGroup>(dsids, limit_events, histo_shape, ev_types, find_bin);
        }
        bool add_event(MuonAnalysis& t) override;

    private:
        std::shared_ptr<TH1> m_histo;
        std::function<int(MuonAnalysis&, TH1*)> m_finder;
        unsigned int m_n_bins;
        unsigned int m_ev_per_bin;
        std::mutex m_mutex_add_event;
    };

    class SampleGroupMgr {
    public:
        SampleGroupMgr();
        void add_sample(const std::vector<int>& dsids, unsigned int limit_events, unsigned ev_types = SampleGroup::EventType::All);
        void add_sample(std::shared_ptr<SampleGroup> grp);

        bool add_event(MuonAnalysis& t);

        /// Returns the typeset mask
        int type_mask(int dsid) const;

        /// returns true if all samples are full
        bool empty() const;

        /// returns whether the dsid is part
        /// of a fillable SampleGroup
        bool is_needed(unsigned int dsid) const;

    private:
        std::vector<std::shared_ptr<SampleGroup>> m_samples;
    };
}  // namespace LeptonTauMVA
#endif
