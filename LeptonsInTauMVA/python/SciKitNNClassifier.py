#!/bin/env/python
from LeptonsInTauMVA.ModelBuilder import setup_basic_parser, fit_model, SciKitModelBuilder
from ClusterSubmission.Utils import FillWhiteSpaces, CreateDirectory
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import numpy as np
import logging
###########################################################################################
#   Use derived classes from the SciKitClassifier for each training type.                 #
#   Through this additional training specific diagnostic plots can be written to the      #
#   diagnostics file                                                                      #
###########################################################################################
# http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html


class SciKitNNClassifier(SciKitModelBuilder):
    def __init__(self, train_options):
        from sklearn.neural_network import MLPClassifier
        class_object = MLPClassifier(hidden_layer_sizes=tuple(train_options.hiddenLayerSizes),
                                     activation=train_options.activation,
                                     solver=train_options.solver,
                                     alpha=train_options.alpha,
                                     batch_size=("auto" if train_options.batchSize == -1 else train_options.batchSize),
                                     learning_rate=train_options.learningRate,
                                     learning_rate_init=train_options.learningRateInit,
                                     power_t=train_options.powerT,
                                     max_iter=train_options.maxIter,
                                     shuffle=(not train_options.noShuffle),
                                     tol=train_options.tol,
                                     warm_start=train_options.warmStart,
                                     momentum=train_options.momentum,
                                     nesterovs_momentum=(not train_options.noNesterovsMomentum),
                                     early_stopping=train_options.earlyStopping,
                                     validation_fraction=train_options.validationFraction,
                                     beta_1=train_options.beta1,
                                     beta_2=train_options.beta2,
                                     epsilon=train_options.epsilon,
                                     verbose=True)

        SciKitModelBuilder.__init__(self,
                                    model=class_object,
                                    HDF5File=train_options.hdf5_file,
                                    outdir=train_options.out_dir,
                                    name=train_options.model_name,
                                    do_cross_valid=train_options.do_cross_valid)
        self.__scaler = StandardScaler()
        self.__scaler_fitted = False

    def _call_mva(self, data):
        return self.model().predict_proba(data)[:, 1]

    def _transform_data(self, data):
        if not self.__scaler_fitted:
            logging.info("<SciKitNNClassifier>: Evaluate the standard mean for later data transformation")
            self.__scaler.fit(data)
            self.__scaler_fitted = True
        return self.__scaler.transform(data)

    def _run_classifier_diagnostics(self):
        self._run_transformed_dist()
        self._run_transformed_dist(use_train_data=False)

    def _run_transformed_dist(self, n_bins=200, n_sigma=3., use_train_data=True):
        range_array = np.array([[self.get_plot_range(n)[0] for n in range(self.num_vars())],
                                [self.get_plot_range(n)[1] for n in range(self.num_vars())]])
        range_array = self._transform_data(range_array)
        for n in range(self.num_vars()):
            val_min = range_array[0, n]
            val_max = range_array[1, n]
            step_size = (val_max - val_min) / n_bins
            binning = np.arange(val_min, val_max + step_size, step_size)
            fig, ax = plt.subplots()

            for class_id in self.dataset().get_out_labels():
                data = self.dataset().get_partial_data(class_id) if use_train_data else self.dataset().get_partial_data_test(class_id)
                ax.hist(x=self._transform_data(data)[:, n],
                        histtype='step',
                        bins=binning,
                        label=self.get_muon_type(class_id),
                        density=True,
                        log=False)
            ax.set_xlabel(self.variable_label(n))
            ax.set_ylabel("a.u.")
            ax.set_title("Normalized mean comparisons (%s)" % ("training" if use_train_data else "testing"))
            ax.legend()
            self._save_plot(fig,
                            self.out_dir() + "diagnostic_plots/Features/NormMean_" + ("Training" if use_train_data else "Testing"),
                            "Distributions_%s" % (self.variable_name(n)))

    def _extra_meta_data(self):
        meta_dict = {}
        for param in sorted(self.model().get_params().iterkeys()):
            meta_dict[param] = self.model().get_params()[param]

        meta_dict["iterations"] = self.model().n_iter_
        meta_dict["final_loss"] = self.model().loss_
        return meta_dict


def setupNNParser():
    parser = setup_basic_parser()
    parser.set_defaults(model_name="NN-MLP")
    ### The training parameters are not optimized yet. They are randomly choosen
    ### Each user must find his best configuration of parameters. The SciKitDiagnositcs class
    ### Is a good starting point to find them out
    parser.add_argument('--hiddenLayerSizes',
                        help='Hidden layer sizes, The ith element represents the number of neurons in the ith hidden layer',
                        default=[15 for x in range(30)],
                        nargs='+',
                        type=int)
    parser.add_argument('--activation',
                        help='Activation function for the hidden layer',
                        type=str,
                        choices=["identity", "logistic", "tanh", "relu"],
                        default="relu")
    parser.add_argument('--solver', help='The solver for weight optimization', type=str, choices=['lbfgs', 'sgd', 'adam'], default='adam')
    parser.add_argument('--alpha', help='L2 penalty (regularization term) parameter', default=0.0001, type=float)
    parser.add_argument(
        "--batchSize",
        help=
        'Size of minibatches for stochastic optimizers. If the solver is "lbfgs", the classifier will not use minibatch. When set to "auto" (-1), batch_size=min(200, n_samples)',
        type=int,
        default=-1)
    parser.add_argument('--learningRate',
                        help='Learning rate schedule for weight updates',
                        type=str,
                        choices=["constant", "invscaling", "adaptive"],
                        default='constant')
    parser.add_argument(
        '--learningRateInit',
        help='The initial learning rate used. It controls the step-size in updating the weights. Only used when solver="sgd" or "adam"',
        default=0.001,
        type=float)
    parser.add_argument(
        '--powerT',
        help=
        'The exponent for inverse scaling learning rate. It is used in updating effective learning rate when the learning_rate is set to "invscaling". Only used when solver="sgd".',
        default=0.5,
        type=float)
    parser.add_argument(
        "--maxIter",
        help=
        'Maximum number of iterations. The solver iterates until convergence (determined by "tol") or this number of iterations. For stochastic solvers ("sgd", "adam"), note that this determines the number of epochs (how many times each data point will be used), not the number of gradient steps.',
        type=int,
        default=800)
    parser.add_argument("--noShuffle",
                        help='Whether to shuffle samples in each iteration. Only used when solver="sgd" or "adam"',
                        default=False,
                        action="store_true")
    parser.add_argument(
        '--tol',
        help=
        'Tolerance for the optimization. When the loss or score is not improving by at least tol for two consecutive iterations, unless learning_rate is set to "adaptive", convergence is considered to be reached and training stops.',
        default=0.0001,
        type=float)
    parser.add_argument(
        "--warmStart",
        help=
        'When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution.',
        default=False,
        action="store_true")
    parser.add_argument('--momentum',
                        help='Momentum for gradient descent update. Should be between 0 and 1. Only used when solver="sgd".',
                        default=0.9,
                        type=float)
    parser.add_argument('--noNesterovsMomentum',
                        help='Whether to use Nesterovs momentum. Only used when solver="sgd" and momentum > 0',
                        default=False,
                        action="store_true")
    parser.add_argument(
        '--earlyStopping',
        help=
        'Whether to use early stopping to terminate training when validation score is not improving. If set to true, it will automatically set aside 10%% of training data as validation and terminate training when validation score is not improving by at least tol for two consecutive epochs. Only effective when solver="sgd" or "adam"',
        default=False,
        action="store_false")
    parser.add_argument(
        '--validationFraction',
        help=
        'The proportion of training data to set aside as validation set for early stopping. Must be between 0 and 1. Only used if early_stopping is True',
        default=0.1,
        type=float)
    parser.add_argument(
        '--beta1',
        help='Exponential decay rate for estimates of first moment vector in adam, should be in [0, 1). Only used when solver="adam"',
        default=0.9,
        type=float)
    parser.add_argument(
        '--beta2',
        help='Exponential decay rate for estimates of second moment vector in adam, should be in [0, 1). Only used when solver="adam"',
        default=0.999,
        type=float)
    parser.add_argument('--epsilon', help='Value for numerical stability in adam. Only used when solver="adam"', default=1e-8, type=float)
    return parser


if __name__ == "__main__":
    train_options = setupNNParser().parse_args()
    nn_class = SciKitNNClassifier(train_options)
    fit_model(nn_class)
