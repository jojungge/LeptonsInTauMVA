from ClusterSubmission.Utils import FillWhiteSpaces
from LeptonsInTauMVA.ModelBuilder import setup_basic_parser, fit_model, SciKitModelBuilder
from sklearn.externals import joblib
import numpy as np
import matplotlib.pyplot as plt
import os, logging


def setupBDTTParser():
    parser = setup_basic_parser()
    parser.set_defaults(model_name="BDT")
    parser.add_argument("--nEstimators", help="Maximum number of estimators at which boosting is terminated", type=int, default=400)
    parser.add_argument("--minSamplesSplit",
                        help="The minimum number of samples required to split an internal node",
                        type=float,
                        default=0.02)
    parser.add_argument("--minSamplesLeaf", help="The minimum number of samples required to be at a leaf node", type=float, default=0.01)
    parser.add_argument("--maxFeatures", help="The number of features to consider when looking for the best split", type=int, default=-1)
    parser.add_argument("--maxDepth", help="How many nodes should be created at each tree", type=int, default=3)
    parser.add_argument("--minNodeSize", help="What is the minimal amount of events needed to build a node", type=float, default=0.03)
    parser.add_argument("--minImpurityDecrease",
                        help="A node will be split if this split induces a decrease of the impurity greater than or equal to this value.",
                        type=float,
                        default=0.0)
    parser.add_argument("--learningRate", help="Learning rate of the AdaBoost classifier", type=float, default=1.0)
    parser.add_argument("--useEntropySplit",
                        help="Use the 'entropy' algorithm as criterion of the best split instead of gini in the TreeClassisfier",
                        action='store_true',
                        default=False)
    parser.add_argument("--useRandomSplit",
                        help="Use a random feature to find the best criterion on the classifier in the TreeClassisfier",
                        action='store_true',
                        default=False)
    parser.add_argument("--useRealBoostingAlg",
                        help="Use the 'SAMME' algorithm in the AdaBoostClassfier else 'SAMME.R'",
                        action='store_true',
                        default=False)
    parser.add_argument("--noWeights", help="Disable event weights during classification", default=False, action="store_true")
    return parser


###########################################################################################
#   Use derived classes from the SciKitModelBuilder for each training type.               #
#   Through this additional training specific diagnostic plots can be written to the      #
#   diagnostics file                                                                      #
###########################################################################################
class SciKitBDTClassifier(SciKitModelBuilder):
    def __init__(self, Options):
        from sklearn.ensemble import AdaBoostClassifier
        from sklearn.tree import DecisionTreeClassifier
        #   For explanation of the parameters c.f.:
        #       http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostClassifier.html
        #       http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html
        #  Each binary tree classifies the data with -1 or 1 depending on whether it's background or not
        bdt = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(
            max_depth=Options.maxDepth,
            min_weight_fraction_leaf=Options.minNodeSize,
            min_impurity_decrease=Options.minImpurityDecrease,
            criterion="gini" if not Options.useEntropySplit else "entropy",
            splitter="best" if not Options.useRandomSplit else "random",
            min_samples_split=Options.minSamplesSplit,
            min_samples_leaf=Options.minSamplesLeaf,
            max_features=Options.maxFeatures if Options.maxFeatures > 0 else None),
                                 algorithm="SAMME" if not Options.useRealBoostingAlg else "SAMME.R",
                                 learning_rate=Options.learningRate,
                                 n_estimators=Options.nEstimators)

        SciKitModelBuilder.__init__(self,
                                    model=bdt,
                                    HDF5File=Options.hdf5_file,
                                    outdir=Options.out_dir,
                                    name=Options.model_name,
                                    do_cross_valid=train_options.do_cross_valid)

    def _run_classifier_diagnostics(self):
        ###
        ###
        self._make_feature_ranking()
        self._make_estimator_errs()

    def _make_feature_ranking(self):
        if len(self.model().feature_importances_) == 0 or np.max(self.model().feature_importances_) == np.min(
                self.model().feature_importances_) or len([x for x in self.model().feature_importances_ if np.isnan(x)]) > 0:
            logging.warning("<SciKitBDTClassifier>: No sensible training could be obtained. Abort the loop")
            return

        Var_Ranking = []
        m_len = 0
        for v in range(self.num_vars()):
            rank = self.model().feature_importances_[v]
            var_name = self.variable_label(v)
            if m_len < len(var_name): m_len = len(var_name)
            Var_Ranking += [(rank, var_name)]

        #### Prompt  the features to screen
        m_len += 5
        i = 1
        Var_Ranking.sort(key=lambda x: x[0], reverse=True)
        logging.info("Feature importances of %s" % (self.name()))
        for rank, var in Var_Ranking:
            logging.info("%2d)       -=-  %s:%s%.6f" % (i, var, FillWhiteSpaces(m_len - len(var)), rank))
            i += 1
        logging.info(FillWhiteSpaces(100, "-#") + FillWhiteSpaces(3, "\n"))
        ### Now make the matplot to save the feature importance
        fig, ax = plt.subplots()
        labels = [x[1] for x in Var_Ranking]
        rankings = np.array([x[0] for x in Var_Ranking])
        ind = np.arange(len(labels))
        width = 0.75
        ax.bar(ind, rankings, width)
        ax.set_xticks(np.arange(len(labels)))
        ax.set_xticklabels(labels)
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
        ax.set_title("Feature ranking %s" % (self.name()))
        self._save_plot(fig, self.out_dir() + "diagnostic_plots/", "%s_feature_ranking" % (self.name()))

    def _make_estimator_errs(self):
        errors = np.array([e for e in self.model().estimator_errors_])
        binning = np.arange(errors.shape[0])
        fig, ax = plt.subplots()
        ax.plot(binning, errors)
        ax.set_xlabel("N-th tree")
        ax.set_ylabel("Error fraction")
        self._save_plot(fig, self.out_dir() + "diagnostic_plots/", "%s_tree_errors" % (self.name()))


###
###
###
if __name__ == "__main__":
    train_options = setupBDTTParser().parse_args()
    bdt_model = SciKitBDTClassifier(train_options)
    fit_model(bdt_model)
