from HDF5Writing.SciKitDataSet import SciKitDataSet
import numpy as np


###
### Specialication of the SciKitDataSet to ease the
### read out of the HDF5 file
class MuonTauDataSet(SciKitDataSet):
    ####    Constructor to open a file
    ####    infile: <path to the HDF5 file>
    ####    use_train_data: Flag whether the train or the testing data is read
    def __init__(self, infile="", use_train_data=True):
        SciKitDataSet.__init__(self, infile=infile)
        self.__use_train_data = use_train_data

    ### Returns the training data. I.e. the features of each muon
    ### to be considered for the analysis
    def get_data(self):
        return np.array(self.getTrainingData(num=self.n_events()) if self.__use_train_data else self.getTestingData(num=self.n_events()))

    ### Returns the classification information. i.e. the associated numbers  to each
    ### muon in consideration
    def get_classifier(self):
        return np.array(
            self.getTrainingClassifier(num=self.n_events()) if self.__use_train_data else self.getTestingClassifier(num=self.n_events()))

    ### Weights can be used to put more emphasis on particular samples compared to the
    ### bulk. In HEP weights are usually used for normalization purposes matching the number of
    ### simulated events to the actually expected events using the total cross-section and the integrated
    ### luminosity divided by the number of all generated samples
    def get_weights(self):
        return np.array(
            self.getTrainingWeights(num=self.n_events()) if self.__use_train_data else self.getTestingWeights(num=self.n_events()))

    ###
    ###
    ###
    def get_partial_data(self, class_id):
        return self.get_data()[self.get_classifier() == class_id]

    def get_complementary_data(self, class_id):
        return self.get_data()[self.get_classifier() != class_id]

    ### Simple helper function to obtain the assigned labels in the analysis
    def get_out_labels(self):
        return np.array(np.unique(self.get_classifier()), dtype=np.int)

    ### How many events are available
    def n_events(self):
        return self.nTestingEvents() if not self.__use_train_data else self.nTrainingEvents()

    #### Get the plot range
    def get_plot_range(self, n, n_sigma=3.):
        if n >= self.nVars():
            raise RuntimeError("%s only has %d variables while you are requsting the %d-th" % (self.file_name(), self.nVars(), n))
        if n_sigma > 0.:
            min_val = min(self.getMinTrainDeviation(n, n_sigma, use_weights=False), self.getMinTestDeviation(n, n_sigma, use_weights=False))

            max_val = min(self.getMaxTrainDeviation(n, n_sigma, use_weights=False), self.getMaxTestDeviation(n, n_sigma, use_weights=False))
            #print self.variableName(n), min_val, max_val, min(self.getTrainingMin(n), self.getTestingMin(n)), max(self.getTrainingMax(n), self.getTestingMax(n))
            return min_val, max_val
        return min(self.getTrainingMin(n), self.getTestingMin(n)), max(self.getTrainingMax(n), self.getTestingMax(n))

    ##
    ### Getters for the testing data
    ##
    def n_events_test(self):
        return self.nTestingEvents() if self.__use_train_data else self.nTrainingEvents()

    def get_data_test(self):
        return np.array(
            self.getTrainingData(num=self.n_events_test()) if not self.__use_train_data else self.getTestingData(num=self.n_events_test()))

    def get_classifier_test(self):
        return np.array(
            self.getTrainingClassifier(num=self.n_events_test()) if not self.__use_train_data else self.getTestingClassifier(
                num=self.n_events_test()))

    def get_partial_data_test(self, class_id):
        return self.get_data_test()[self.get_classifier_test() == class_id]

    def get_complementary_data_test(self, class_id):
        return self.get_data_test()[self.get_classifier_test() != class_id]


###
####  transforms the y labels to a binary
###   score, where the target_id is set to be one and the
###   rest is 0
def to_binary(y_in, target_id):
    y_out = np.copy(y_in)
    y_out[y_out == target_id] = -1
    y_out[(y_out != target_id) & (y_out >= 0)] = 0
    y_out[y_out == -1] = 1
    return y_out


###
###     Dummy testing routine
###
if __name__ == "__main__":
    my_ds = MuonTauDataSet(infile="/ptmp/mpp/junggjo9/ForDaniel/outfile.H5", use_train_data=True)
    from pprint import pprint
    class_labels = my_ds.get_out_labels()
    for x in class_labels:
        partial_data = my_ds.get_partial_data(x)
        print partial_data.shape
        pprint(partial_data)
        pprint(my_ds.get_data())
        pprint(my_ds.n_events())
        pprint(my_ds.get_classifier())
        pprint(my_ds.get_out_labels())
