#include <HDF5Writing/HDF5ClassFactory.h>
#include <LeptonsInTauMVA/HDF5Loop.h>

#include <fstream>
#include <iostream>
#include <string>

namespace {

    const std::vector<int> V_boson_range = LeptonTauMVA::make_range(303437, 303455) + LeptonTauMVA::make_range(800302, 800320) +
                                           LeptonTauMVA::make_range(361108, 361107) + LeptonTauMVA::make_range(361101, 361104) +
                                           LeptonTauMVA::make_range(301140, 301178);
}
void configure_mgr(LeptonTauMVA::SampleGroupMgr& grp_mgr, std::shared_ptr<TH1> flat_h,
                   std::function<int(LeptonTauMVA::MuonAnalysis& t, TH1*)> find_bin, unsigned int max_events);
void configure_mgr(LeptonTauMVA::SampleGroupMgr& grp_mgr, unsigned int max_events);
int main(int argc, char* argv[]) {
    // Some variables for configuration
    NtupleBranchBase::setTreeCacheSize(2);
    /// Name of the ouptut file
    std::string out_file = "train_out_file.H5";
    /// Name of the input tree
    std::string tree_name = "MuonTree";
    /// Vector of the files to run
    std::vector<std::string> in_files;

    int max_events = -1;
    int max_train_events = 2.e5;
    int max_test_events = 2.e5;
    unsigned int nThreads = 1;
    /// read the config provided by the user
    for (int k = 1; k < argc; ++k) {
        std::string the_arg(argv[k]);
        if (the_arg.find("-t") == 0 || the_arg.find("--treeName") == 0) {
            tree_name = argv[k + 1];
            ++k;
        }
        /// Specify the input file directly
        else if (the_arg.find("-i") == 0 || the_arg.find("--inFile") == 0) {
            in_files.push_back(argv[k + 1]);
            ++k;
        }
        /// Specify a directory in which all the input files are stored
        else if (the_arg.find("-d") == 0 || the_arg.find("--dir") == 0) {
            in_files += LeptonTauMVA::ListDirectory(PlotUtils::GetFilePath(argv[k + 1]), ".root");
            ++k;
        }
        /// Specify a list of files
        else if (the_arg.find("-f") == 0 || the_arg.find("--fileList") == 0) {
            in_files += LeptonTauMVA::ReadList(PlotUtils::GetFilePath(argv[k + 1]));
        } else if (the_arg.find("--listDir") == 0) {
            std::vector<std::string> in_dir = LeptonTauMVA::ListDirectory(PlotUtils::GetFilePath(argv[k + 1]), ".txt");
            for (auto& in : in_dir) { in_files += LeptonTauMVA::ReadList(in); }
        }
        /// Change the name of the out_file
        else if (the_arg.find("-o") == 0 || the_arg.find("--outFile") == 0) {
            out_file = argv[k + 1];
            ++k;
        }
        /// Change the number of events
        else if (the_arg.find("--maxEvents") == 0) {
            max_events = atoi(argv[k + 1]);
        }
    }

    /// Basic checks
    if (in_files.empty()) {
        std::cerr << "No files were given." << std::endl;
        return EXIT_FAILURE;
    }
    if (tree_name.empty()) {
        std::cerr << "What tree to analyze?" << std::endl;
        return EXIT_FAILURE;
    }

    std::shared_ptr<LeptonTauMVA::EventCounter> ev_counter = std::make_shared<LeptonTauMVA::EventCounter>(in_files, tree_name);
    if (ev_counter->num_files() == 0) return EXIT_FAILURE;

    nThreads = std::min(nThreads, std::thread::hardware_concurrency());
    ;
    std::cout << "In total, " << ev_counter->tot_events() << " will be processed." << std::endl;
    std::cout
        << "##############################################################################################################################"
        << std::endl;
    std::cout
        << "##############################################################################################################################"
        << std::endl;
    /// Prepare now the output file format
    std::shared_ptr<XAMPP::HDF5ClassFactory> out_file_writer = std::make_shared<XAMPP::HDF5ClassFactory>();
    if (out_file.rfind("/") != std::string::npos) {
        out_file_writer->setDirectory(out_file.substr(0, out_file.rfind("/")));
    } else {
        out_file_writer->setDirectory("");
    }
    out_file_writer->setFileName(out_file.substr(out_file.rfind("/") + 1));

    /// Define the training variables. Important notice
    /// there is no check if the names actually match the meaning
    /// be carful in assigning sensible names and also that the variables
    /// are later filled in exactly the corresponing order
    for (std::string var_name : {
             "Pt",
             "AbsEta",
             "RhoPrime",
             "QoverPSig",
             "D0",
             "D0Sig",
             "Z0SinTheta",
             "DeltaZ0",
             "TrackIso",
             "CaloIso",
             //"NTackPt1p5",
             //"NTrackPt5",
             //"VtxSumPt2",
             "IDTrks_Chi2nDoF",
             "CBTrks_Chi2nDoF",
             "Z0Sig",
             "RelPtErr",
             "MomBalSig",
             "Sig_d0z0",
             "Sig_d0theta",
             "Sig_d0QoverP",
             "Sig_z0theta",
             "SigmaChi2OS",
             "RadFirstHit",
         }) {
        out_file_writer->addVariable(var_name);
    }
    ///
    ///
    ///
    for (int i = 0; i < LeptonTauMVA::PerigeeParams::num_perigess; ++i) {
        for (int j = 0; j <= i; ++j) {
            out_file_writer->addVariable("IDTrks_cov_" + to_string(LeptonTauMVA::PerigeeParams(i), LeptonTauMVA::PerigeeParams(j)));
        }
    }
    // for (int i = 0; i < LeptonTauMVA::PerigeeParams::num_perigess; ++i) {
    //    for (int j = 0; j <= i; ++j) {
    //        out_file_writer->addVariable("CBTrks_cov_" + to_string(LeptonTauMVA::PerigeeParams(i), LeptonTauMVA::PerigeeParams(j)));
    //    }
    // }

    auto fill_func = [](LeptonTauMVA::MuonAnalysis& ana_tree) -> std::vector<double> {
        return std::vector<double>{
                   ana_tree.Muons_pt() * LeptonTauMVA::MeVtoGeV,  /// Lepton pt

                   std::abs(ana_tree.Muons_eta()),             /// Absolute of the eta
                   ana_tree.rho_prime(),                       /// rho prime variable
                   std::abs(ana_tree.qover_p_significance()),  /// rho prime variable

                   std::abs(ana_tree.CBTracks_d0()),                                 /// D0 of the muon
                   std::abs(ana_tree.d0_significance()),                             /// d0 significance of the muon
                   std::abs(ana_tree.CBTracks_z0_primary() * ana_tree.sin_theta()),  /// z0 sintheta of the muon
                   std::abs(ana_tree.muon_vtx_z0()),  /// displacement between the primary vertex and the muon vertex
                   ana_tree.track_iso(),              /// Track isolation
                   ana_tree.calo_iso(),               /// Calorimeter isolation
                   // double(ana_tree.IDTracks_assocVtx_NTrksPt1500()),  /// Number of tracks with pT> 1.5 GeV associated with the muon
                   /// production vertex
                   // double(ana_tree.IDTracks_assocVtx_NTrksPt5000()),  /// Number of tracks with pT> 5 GeV associated with the muon
                   /// procution vertex
                   // ana_tree.IDTracks_assocVtx_SumPt2() * LeptonTauMVA::MeVtoGeV,
                   /// Take the improved PLV tagger
                   /// ana_tree.Muons_PromptLeptonRNN_prompt(),
                   /// ana_tree.Muons_PromptLeptonImprovedInput_CandVertex_normDistToPriVtxLongitudinalBest(),
                   ana_tree.ID_chi2OverNDoF(),
                   ana_tree.CB_chi2OverNDoF(),
                   std::abs(ana_tree.z0_significance()),
                   ana_tree.rel_pt_err(),
                   std::abs(ana_tree.Muons_momentumBalanceSignificance()),
                   ana_tree.ID_perigee_sig(LeptonTauMVA::PerigeeParams::d0, LeptonTauMVA::PerigeeParams::z0),
                   ana_tree.ID_perigee_sig(LeptonTauMVA::PerigeeParams::d0, LeptonTauMVA::PerigeeParams::theta),

                   ana_tree.ID_perigee_sig(LeptonTauMVA::PerigeeParams::d0, LeptonTauMVA::PerigeeParams::qOverP),
                   ana_tree.ID_perigee_sig(LeptonTauMVA::PerigeeParams::z0, LeptonTauMVA::PerigeeParams::theta),

                   double(ana_tree.IDTracks_standardDeviationOfChi2OS()),
                   double(ana_tree.IDTracks_radiusOfFirstHit()),
               } +
               ana_tree.sqrt_ID_cov_vec();
        //+ ana_tree.CB_perigee_sig_vec();
    };

    std::shared_ptr<LeptonTauMVA::SampleGroupMgr> train_samples = std::make_shared<LeptonTauMVA::SampleGroupMgr>();
    std::shared_ptr<LeptonTauMVA::SampleGroupMgr> test_samples = std::make_shared<LeptonTauMVA::SampleGroupMgr>();
    configure_mgr(
        *train_samples, std::make_shared<TH1I>("flattener", "dummy", 75, 10, 160),
        [](LeptonTauMVA::MuonAnalysis& t, TH1* h) {
            int bin_x = h->GetXaxis()->FindBin(t.Muons_pt() * LeptonTauMVA::MeVtoGeV);
            // bin_x -= isOverflowBin(h->GetXaxis(),bin_x);
            // int bin_y = h->GetYaxis()->FindBin(std::abs(t.Muons_eta()));
            return h->GetBin(bin_x);
        },
        max_train_events);
    configure_mgr(*test_samples,
                  // std::make_shared<TH1I>("flattener", "dummy", 75, 10, 160),
                  //[](LeptonTauMVA::MuonAnalysis& t, TH1* h) {
                  //    int bin_x = h->GetXaxis()->FindBin(t.Muons_pt() * LeptonTauMVA::MeVtoGeV);
                  //    // bin_x -= isOverflowBin(h->GetXaxis(),bin_x);
                  //    // int bin_y = h->GetYaxis()->FindBin(std::abs(t.Muons_eta()));
                  //    return h->GetBin(bin_x);
                  //},
                  max_test_events);

    std::cout << " Start loop over the files " << std::endl;

    std::vector<std::future<Long64_t>> threads;
    std::vector<std::shared_ptr<LeptonTauMVA::HDF5Loop>> loops;
    threads.reserve(in_files.size());
    for (unsigned int n = 0; n < nThreads; ++n) {
        loops.push_back(std::make_shared<LeptonTauMVA::HDF5Loop>(out_file_writer, train_samples, test_samples, ev_counter));
        std::shared_ptr<LeptonTauMVA::HDF5Loop> my_loop = loops.back();
        threads.emplace_back(std::async(std::launch::async, &LeptonTauMVA::HDF5Loop::start_threads, my_loop.get(), fill_func));
    }
    unsigned int n = LeptonTauMVA::count_active(threads);
    while (n > 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(5000));
        n = LeptonTauMVA::count_active(threads);
    }
    return EXIT_SUCCESS;
}

void configure_mgr(LeptonTauMVA::SampleGroupMgr& grp_mgr, std::shared_ptr<TH1> flat_h,
                   std::function<int(LeptonTauMVA::MuonAnalysis& t, TH1*)> find_bin, unsigned int max_events) {
    using namespace LeptonTauMVA;
    grp_mgr.add_sample(FlatlShapeGroup::make_shared(V_boson_range, max_events, flat_h, SampleGroup::PromptMuon, find_bin));
    grp_mgr.add_sample(FlatlShapeGroup::make_shared(V_boson_range, max_events, flat_h, SampleGroup::PromptMuonFromTau, find_bin));

    /// Include the High-Mass drell Yan samples to gain statistics at high momenta
    grp_mgr.add_sample(FlatlShapeGroup::make_shared({410470}, max_events / 4, flat_h, SampleGroup::PromptMuonFromTau, find_bin));
    grp_mgr.add_sample(FlatlShapeGroup::make_shared({410470}, max_events / 4, flat_h, SampleGroup::PromptMuon, find_bin));
    // grp_mgr.add_sample(FlatlShapeGroup::make_shared({410470}, 1.25 * max_events, flat_h, SampleGroup::Background, find_bin));
}
void configure_mgr(LeptonTauMVA::SampleGroupMgr& grp_mgr, unsigned int max_events) {
    using namespace LeptonTauMVA;
    grp_mgr.add_sample(V_boson_range, max_events, LeptonTauMVA::SampleGroup::PromptMuon);
    grp_mgr.add_sample(V_boson_range, max_events, LeptonTauMVA::SampleGroup::PromptMuonFromTau);

    /// 250k muons from ttbar
    grp_mgr.add_sample({410470}, max_events / 4, LeptonTauMVA::SampleGroup::PromptMuonFromTau);
    grp_mgr.add_sample({410470}, max_events / 4, LeptonTauMVA::SampleGroup::PromptMuon);
    /// Background muons from ttbar
    // grp_mgr.add_sample({410470}, 2 * max_events, LeptonTauMVA::SampleGroup::Background);
}
