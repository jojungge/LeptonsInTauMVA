#include <LeptonsInTauMVA/MuonAnalysis.h>
#include <LeptonsInTauMVA/SampleGroup.h>
#include <LeptonsInTauMVA/Utils.h>
#include <NtupleAnalysisUtils/PlotUtils.h>
#include <TH1I.h>
#include <TH2I.h>
#include <TH3I.h>

#include <fstream>
#include <iostream>
#include <string>

using namespace LeptonTauMVA;
int main(int argc, char* argv[]) {
    SetAtlasStyle();
    HistoFiller::getDefaultFiller()->setNThreads(8);

    // Some variables for configuration

    /// Name of the ouptut file
    std::string out_dir = "feature_plots/";
    /// Name of the input tree
    std::string tree_name = "MuonTree";
    /// Vector of the files to run
    std::vector<std::string> in_files;

    /// read the config provided by the user
    for (int k = 1; k < argc; ++k) {
        std::string the_arg(argv[k]);
        if (the_arg.find("-t") == 0 || the_arg.find("--treeName") == 0) {
            tree_name = argv[k + 1];
            ++k;
        }
        /// Specify the input file directly
        else if (the_arg.find("-i") == 0 || the_arg.find("--inFile") == 0) {
            in_files.push_back(argv[k + 1]);
            ++k;
        }
        /// Specify a directory in which all the input files are stored
        else if (the_arg.find("-d") == 0 || the_arg.find("--dir") == 0) {
            in_files += LeptonTauMVA::ListDirectory(PlotUtils::GetFilePath(argv[k + 1]), ".root");
            ++k;
        }
        /// Specify a list of files
        else if (the_arg.find("-f") == 0 || the_arg.find("--fileList") == 0) {
            in_files += LeptonTauMVA::ReadList(PlotUtils::GetFilePath(argv[k + 1]));
        } else if (the_arg.find("--listDir") == 0) {
            std::vector<std::string> in_dir = LeptonTauMVA::ListDirectory(PlotUtils::GetFilePath(argv[k + 1]), ".txt");
            for (auto& in : in_dir) { in_files += LeptonTauMVA::ReadList(in); }
        }
        /// Change the name of the out_file
        else if (the_arg.find("-o") == 0 || the_arg.find("--outDir") == 0) {
            out_dir = argv[k + 1];
            ++k;
        }
    }
    in_files = get_good_files(in_files, tree_name);
    /// Basic checks
    if (in_files.empty()) {
        std::cerr << "No files were given." << std::endl;
        return EXIT_FAILURE;
    }
    if (tree_name.empty()) {
        std::cerr << "What tree to analyze?" << std::endl;
        return EXIT_FAILURE;
    }

    /// Create the sample handler
    Sample<MuonAnalysis> mySample("SampleNameNotUsedHere");

    /// Check that the files are
    for (auto& f_path : in_files) { mySample.addFile(f_path, tree_name); }
    /// Vector to cache the feature plots
    std::vector<PlotContent<TH1D>> features_to_draw;

    CanvasOptions canvas_opts = CanvasOptions()
                                    .yAxisTitle("a.u.")
                                    .labelStatusTag("Simulation Internal")
                                    .logY(false)
                                    .yMaxExtraPadding(0.7)
                                    .overrideOutputDir(out_dir);

    /// Define the function to create the feature plots
    auto add_feature_plot = [&features_to_draw, &mySample, &canvas_opts](const PlotFiller1D& filler, const Cut& sel,
                                                                         const std::string& extra_label = "") {
        /// Selection criteria
        static const Cut prompt_muons("Prompt_muons", [](MuonAnalysis& t) { return t.classify_muon() == SampleGroup::b_PromptMuon; });
        static const Cut tau_muons("Tau_muons", [](MuonAnalysis& t) { return t.classify_muon() == SampleGroup::b_PromptMuonFromTau; });
        static const Cut bkg_muons("Bkg_muons", [](MuonAnalysis& t) { return t.classify_muon() == SampleGroup::b_Background; });
        /// Make sure to normalize the plots to unity
        static const PlotPostProcessor<TH1D, TH1D> normaliseToUnitIntegral("NormToUnit", [](std::shared_ptr<TH1D> in) {
            std::cout << in->GetName() << " " << Form("%.0f", in->GetEntries()) << std::endl;
            pull_overflow(in);
            in->Scale(1. / in->Integral());
            return in;
        });

        features_to_draw.push_back(PlotContent<TH1D>(
            {
                Plot<TH1D>(mySample, prompt_muons && sel, filler, normaliseToUnitIntegral, "prompt #mu", "PL",
                           PlotFormat().Color(kOrange - 3).FillStyle(1001).FillAlpha(0.5).ExtraDrawOpts("HIST")),
                Plot<TH1D>(mySample, tau_muons && sel, filler, normaliseToUnitIntegral, "prompt #mu (w intermediate #tau)", "PL",
                           PlotFormat().Color(kRed).FillStyle(1001).FillAlpha(0.5).ExtraDrawOpts("HIST")),
                // Plot<TH1D>(mySample, bkg_muons && sel, filler, normaliseToUnitIntegral, "background #mu", "PL",
                //            PlotFormat().Color(kBlue).FillStyle(1001).FillAlpha(0.5).ExtraDrawOpts("HIST")),
            },
            ///
            {"X#rightarrow#tau#rightarrow#mu vs. X#rightarrow#mu", extra_label}, "FeaturePlot_" + filler.getName() + sel.getName(),
            "AllFeaturePlots", canvas_opts));
    };

    int max_train_events = 1.e6;
    const std::vector<int> V_boson_range = make_range(303437, 303455) + make_range(800302, 800320) + make_range(361108, 361107) +
                                           make_range(361101, 361104) + make_range(301140, 301178);

    auto configure_mgr = [&max_train_events, &V_boson_range](SampleGroupMgr& grp_mgr, std::shared_ptr<TH1> flat_h,
                                                             std::function<int(MuonAnalysis & t, TH1*)> find_bin) {
        grp_mgr.add_sample(FlatlShapeGroup::make_shared(V_boson_range, max_train_events, flat_h, SampleGroup::PromptMuon, find_bin));
        grp_mgr.add_sample(FlatlShapeGroup::make_shared(V_boson_range, max_train_events, flat_h, SampleGroup::PromptMuonFromTau, find_bin));

        // grp_mgr.add_sample(FlatlShapeGroup::make_shared({410470}, max_train_events / 4, flat_h, SampleGroup::PromptMuonFromTau,
        // find_bin));
        //  grp_mgr.add_sample(FlatlShapeGroup::make_shared({410470}, max_train_events / 4, flat_h, SampleGroup::PromptMuon, find_bin));
        //  grp_mgr.add_sample(FlatlShapeGroup::make_shared({410470}, 2 * max_train_events, flat_h, SampleGroup::Background, find_bin));
    };

    std::shared_ptr<SampleGroupMgr> grp_mgr_eta = std::make_shared<SampleGroupMgr>();
    std::shared_ptr<SampleGroupMgr> grp_mgr_pt = std::make_shared<SampleGroupMgr>();
    std::shared_ptr<SampleGroupMgr> grp_mgr_pteta = std::make_shared<SampleGroupMgr>();

    configure_mgr(*grp_mgr_eta, std::make_shared<TH1I>("flattener", "dummy", 50, -2.5, 2.5),
                  [](MuonAnalysis& t, TH1* h) { return h->GetXaxis()->FindBin(t.Muons_eta()); });

    configure_mgr(*grp_mgr_pt, std::make_shared<TH1I>("flattener", "dummy", 50, 10, 160),
                  [](MuonAnalysis& t, TH1* h) { return h->GetXaxis()->FindBin(t.Muons_pt() * MeVtoGeV); });

    configure_mgr(*grp_mgr_pteta, std::make_shared<TH2I>("flattener", "dummy", 25, 10, 160, 25, 0, 2.5), [](MuonAnalysis& t, TH1* h) {
        int bin_x = h->GetXaxis()->FindBin(t.Muons_pt() * MeVtoGeV);
        // bin_x -= isOverflowBin(h->GetXaxis(),bin_x);
        int bin_y = h->GetYaxis()->FindBin(std::abs(t.Muons_eta()));
        return h->GetBin(bin_x, bin_y);
    });

    //
    std::vector<std::pair<Cut, std::string>> selections{
        // std::make_pair(Cut("FlatEta", [grp_mgr_eta](MuonAnalysis& t) { return grp_mgr_eta->add_event(t); }), "flat in #eta"),
        std::make_pair(Cut("FlatPt", [grp_mgr_pt](MuonAnalysis& t) { return grp_mgr_pt->add_event(t); }), "flat in p_{T}"),
        // std::make_pair(Cut("FlatPtEta", [grp_mgr_pteta](MuonAnalysis& t) { return grp_mgr_pteta->add_event(t); }),
        //               "flat in p_{T} #wedge #eta"),
        // std::make_pair(Cut("", [](MuonAnalysis&) { return true; }), "inclusive"),

    };
    /// Call the function to create the feature plots
    /// The first argument is the name of the plot
    /// The second one is the instruction how the histograms are going to be filled
    /// The next arguments are constructing the histogram
    for (auto& sel_pair : selections) {
        add_feature_plot(PlotFiller1D(
                             "Muon_Pt", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.Muons_pt() * MeVtoGeV); }, "Muon_PtHisto",
                             "dummy;p_{T} [GeV];a.u.", 38, 10, 200),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_Eta", [](TH1D* h, MuonAnalysis& t) { h->Fill(std::fabs(t.Muons_eta())); }, "Muon_EtaHisto",
                             "dummy;|#eta|;a.u.", 25, 0, 2.5),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_RhoPrime", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.rho_prime()); }, "Muon_RhoPrimeHisto",
                             "dummy;#rho^{'};a.u.", 21, -1.05, 1.05),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_QoverPSig", [](TH1D* h, MuonAnalysis& t) { h->Fill(std::fabs(t.qover_p_significance())); },
                             "Muon_QoverPSignficance", "dummy;Z(#frac{q}{p});a.u.", 50, 0, 5),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_d0", [](TH1D* h, MuonAnalysis& t) { h->Fill(std::fabs(t.CBTracks_d0())); }, "Muon_d0Histo",
                             "dummy;|d_{0}| [mm];a.u.", 15, 0, 1.5),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_d0sign", [](TH1D* h, MuonAnalysis& t) { h->Fill(std::fabs(t.d0_significance())); }, "Muon_d0sig",
                             "dummy;#frac{|d_{0}|}{#sigma(d_{0})};a.u.", 20, 0, 5),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_z0sinT", [](TH1D* h, MuonAnalysis& t) { h->Fill(std::fabs(t.CBTracks_z0_primary() * t.sin_theta())); },
                             "Muon_zst", "dummy;|z_{0}sin#theta| [mm];a.u.", 15, 0, 1.5),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_trackIso", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.track_iso()); }, "Muon_trackIso",
                             "dummy;track isolation;a.u.", 11, 0, 1.1),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_caloIso", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.calo_iso()); }, "Muon_caloIso",
                             "dummy;calorimeter isolation;a.u.", 12, -0.1, 1.1),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_dZ", [](TH1D* h, MuonAnalysis& t) { h->Fill(std::abs(t.muon_vtx_z0())); }, "Muon_dZ",
                             "dummy;|#Delta z_{0}^{primary} - z_{0}^{associated}| [mm];a.u.", 10, 0, 0.5),
                         sel_pair.first, sel_pair.second);

        add_feature_plot(PlotFiller1D(
                             "Muon_NTracks_assocPt5000", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.IDTracks_assocVtx_NTrksPt5000()); },
                             "Muon_NTracks_assocPt5000", "dummy;N_{tracks}^{p_{T} > 5 GeV};a.u.", 10, 0, 11),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_NTracks_assocPt1500", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.IDTracks_assocVtx_NTrksPt1500()); },
                             "Muon_NTracks_assocPt1500", "dummy;N_{tracks}^{p_{T} > 1.5 GeV};a.u.", 40, 0, 40),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(
            PlotFiller1D(
                "Muon_MomentumBalanceSig", [](TH1D* h, MuonAnalysis& t) { h->Fill(std::abs(t.Muons_momentumBalanceSignificance())); },
                "Muon_MomentumBalanceSig", "dummy;#sigma( p_{T} balanace);a.u.", 40, 0, 1.),
            sel_pair.first, sel_pair.second);

        add_feature_plot(PlotFiller1D(
                             "Chi2_nDoF_ID", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.ID_chi2OverNDoF()); }, "Chi2_nDoF_ID",
                             "dummy;#chi^{2}/nDoF (ID tracks);a.u.", 20, 0, 5.),
                         sel_pair.first, sel_pair.second);

        add_feature_plot(PlotFiller1D(
                             "Chi2_nDoF_MS", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.MS_chi2OverNDoF()); }, "Chi2_nDoF_MS",
                             "dummy;#chi^{2}/nDoF (MS tracks);a.u.", 20, 0, 5.),
                         sel_pair.first, sel_pair.second);

        add_feature_plot(PlotFiller1D(
                             "Chi2_nDoF_CB", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.CB_chi2OverNDoF()); }, "Chi2_nDoF_CB",
                             "dummy;#chi^{2}/nDoF (CB tracks);a.u.", 20, 0, 5.),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "Muon_AssocTrackPt", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.IDTracks_assocVtx_SumPt2() * MeVtoGeV); },
                             "Muon_AssocTrackPt", "dummy;#sqrt{#sum{tracks} p_{T}} [GeV] ;a.u.", 20, 0, 200),
                         sel_pair.first, sel_pair.second);

        add_feature_plot(PlotFiller1D(
                             "ID_pixelDE_dX", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.IDTracks_pixeldEdx()); }, "ID_pixelDE_dX",
                             "dummy;#frac{#partialE}{#partialx} [GeV/mm] - ID pixels ;a.u.", 20, 0, 2.),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "ID_radiusFirstHit", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.IDTracks_radiusOfFirstHit()); },
                             "ID_radiusFirstHit", "dummy; R_{first hit} [mm] ;a.u.", 50, 30, 80.),
                         sel_pair.first, sel_pair.second);

        add_feature_plot(PlotFiller1D(
                             "ID_ptErr", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.rel_pt_err()); }, "ID_ptErr",
                             "dummy;rel error on p_{T};a.u.", 40, 0, 0.25),
                         sel_pair.first, sel_pair.second);
        add_feature_plot(PlotFiller1D(
                             "ID_z0Significance", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.z0_significance()); }, "ID_z0Significance",
                             "dummy; #frac{z_{0}}{#sigma_{z_{0}}} ;a.u.", 50, 0, 10.),
                         sel_pair.first, sel_pair.second);

        add_feature_plot(PlotFiller1D(
                             "ID_SigmaOfChi2OS", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.IDTracks_standardDeviationOfChi2OS()); },
                             "ID_SigmaOfChi2OS", "dummy;#sigma of #chi2 OS;a.u.", 200, 0, 200.),
                         sel_pair.first, sel_pair.second);

        // add_feature_plot(PlotFiller1D(
        //                      "PLV_Vtx",
        //                      [](TH1D* h, MuonAnalysis& t) {
        //                          float val = t.Muons_PromptLeptonImprovedInput_CandVertex_normDistToPriVtxLongitudinalBest();
        //                          h->Fill(val);
        //                      },
        //                      "PLV_Vtx", "dummy;PLV Vertex", 25, -25, 25),
        //                  sel_pair.first, sel_pair.second);
        // add_feature_plot(PlotFiller1D(
        //                      "PLV_PromtRNN", [](TH1D* h, MuonAnalysis& t) { h->Fill(t.Muons_PromptLeptonRNN_prompt()); }, "PLV_Vtx",
        //                      "dummy;PLV RNN", 25, -1, 1),
        //                  sel_pair.first, sel_pair.second);
    }

    std::sort(features_to_draw.begin(), features_to_draw.end(),
              [](const PlotContent<TH1D>& a, const PlotContent<TH1D>& b) { return a.getFileName() < b.getFileName(); });
    PlotUtils::startMultiPagePdfFile("AllFeaturePlots", out_dir);
    for (auto& pc : features_to_draw) { DefaultPlotting::draw1DNoRatio(pc); }
    PlotUtils::endMultiPagePdfFile("AllFeaturePlots", out_dir);

    return EXIT_SUCCESS;
}
