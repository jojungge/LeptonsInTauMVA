#include <LeptonsInTauMVA/HDF5Loop.h>
#include <LeptonsInTauMVA/Utils.h>
namespace {
    std::mutex filling_locker;

}
namespace LeptonTauMVA {
    HDF5Loop::HDF5Loop(std::shared_ptr<XAMPP::HDF5ClassFactory> factory, std::shared_ptr<SampleGroupMgr> train_sample,
                       std::shared_ptr<SampleGroupMgr> test_sample, std::shared_ptr<EventCounter> ev_counter) :
        m_factory(factory), m_train_sample(train_sample), m_test_sample(test_sample), m_ev_counter(ev_counter) {}

    Long64_t HDF5Loop::process_file(std::shared_ptr<TFile> r_file, const std::string& tree_name,
                                    std::function<std::vector<double>(MuonAnalysis&)> var_filler) {
        if (!r_file) { return -1; }
        TTree* t = nullptr;
        r_file->GetObject(tree_name.c_str(), t);
        if (!t) {
            std::cerr << " File does not contain the tree: " << tree_name << std::endl;
            return -1;
        }
        MuonAnalysis ana_tree(t);
        const Long64_t ev_in_tree = ana_tree.getEntries();
        const Long64_t print_interval = std::max(ev_in_tree / 200, Long64_t(10000));
        Long64_t e = 0;
        bool cleared = true;
        for (; e < ev_in_tree; ++e) {
            // if (e == 0) std::cout << "Start to load the first event " << cpu_time << ";" << std::endl;
            ana_tree.getEntry(e);
            // if (e == 0) std::cout << "First event loaded " << cpu_time << ";" << std::endl;
            /// Decide about the classification
            unsigned int class_id = ana_tree.classify_muon();
            // if (e == 0) std::cout << " event classification made " << cpu_time << ";" << std::endl;
            bool train_ev = m_train_sample->add_event(ana_tree);
            bool test_ev = !train_ev && m_test_sample->add_event(ana_tree);
            // if (e == 0) std::cout << " Found the appropiate sample " << cpu_time << ";" << std::endl;

            if (train_ev || test_ev) {
                /// The event weight can be used to put more emphasis on a given sample.
                /// For the moment this variable is just a dummy entry
                double ev_weight = 1.;
                /// Split the data between training and testing
                if ((train_ev && !m_factory->AddTrainingEvent(var_filler(ana_tree), ev_weight, class_id)) ||
                    (test_ev && !m_factory->AddTestingEvent(var_filler(ana_tree), ev_weight, class_id))) {
                    std::cerr << "Filling failed" << std::endl;
                    m_factory->close();
                    m_factory.reset();
                    return -1;
                }
                // if (e == 0) std::cout << " Wrote the first event " << cpu_time << ";" << std::endl;
                cleared = false;
            } else if (!cleared) {
                /// Basic assumption here that the files are not merged across the DSIDs
                int dsid = ana_tree.mcChannelNumber();
                if (!m_train_sample->is_needed(dsid) && !m_test_sample->is_needed(dsid)) { break; }
                auto active = ana_tree.get_active_branches();
                for (auto& to_deact : active) { to_deact->detach(); }
                cleared = true;
            }
            if (m_train_sample->empty() && m_test_sample->empty()) { break; }
            if (e > 0 && e % print_interval == 0) { m_ev_counter->update_counter(print_interval); }
        }
        m_ev_counter->finish_file(ev_in_tree, e, print_interval);
        delete t;
        return e;
    }
    Long64_t HDF5Loop::process_file(std::shared_ptr<TFile> r_file, std::function<std::vector<double>(MuonAnalysis&)> var_filler) {
        return process_file(r_file, m_ev_counter->tree(), var_filler);
    }
    Long64_t HDF5Loop::process_file(const std::string& file_name, std::function<std::vector<double>(MuonAnalysis&)> var_filler) {
        return process_file(PlotUtils::Open(file_name), var_filler);
    }

    Long64_t HDF5Loop::start_threads(std::function<std::vector<double>(MuonAnalysis&)> var_filler) {
        std::shared_ptr<TFile> next_file = m_ev_counter->open_next();
        Long64_t proc_ev = 0;
        while (next_file && !(m_train_sample->empty() && m_test_sample->empty())) {
            Long64_t proc_in_file = process_file(next_file, var_filler);
            if (proc_in_file < 0) { return proc_in_file; }
            proc_ev += proc_in_file;
            next_file = m_ev_counter->open_next();
        }
        return proc_ev;
    }
}  // namespace LeptonTauMVA
