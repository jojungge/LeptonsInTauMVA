#include <LeptonsInTauMVA/EventCounter.h>
#include <LeptonsInTauMVA/Utils.h>

namespace LeptonTauMVA {
    EventCounter::EventCounter(const std::vector<std::string>& in_files, const std::string& tree_name) :
        m_in_files(),
        m_tree_name(tree_name),
        m_file_counter(0),
        m_files_finished(0),
        m_next_mutex(),
        m_proc_ev_mutex(),
        m_tot_ev(0),
        m_prompt_ev(1),
        m_proc_ev(0),
        m_watch(),
        m_watch_started(false) {
        m_in_files = LeptonTauMVA::get_good_files(in_files, tree_name, m_tot_ev);
        m_prompt_ev = m_tot_ev / 200.;
        if (m_prompt_ev < 10000) m_prompt_ev = 10000;
    }
    std::string EventCounter::tree() const { return m_tree_name; }
    size_t EventCounter::num_files() const { return m_in_files.size(); }
    Long64_t EventCounter::tot_events() const { return m_tot_ev; }
    Long64_t EventCounter::proc_events() const { return m_proc_ev; }

    std::string EventCounter::next() {
        std::lock_guard<std::mutex> guard(m_next_mutex);
        if (!m_watch_started) {
            m_watch.Start();
            m_watch_started = true;
        }
        if (m_file_counter == num_files()) return std::string("");
        std::string to_ret = m_in_files[m_file_counter];
        ++m_file_counter;
        // std::cout<<"Open file ("<<m_file_counter<<"/"<<num_files()<<"): "<<to_ret<<std::endl;
        return to_ret;
    }
    std::shared_ptr<TFile> EventCounter::open_next() {
        std::string next_file = next();
        if (next_file.empty()) return nullptr;
        std::shared_ptr<TFile> f = PlotUtils::Open(next_file);
        if (!f) return open_next();
        return f;
    }
    void EventCounter::update_counter(Long64_t to_update) {
        std::lock_guard<std::mutex> guard(m_proc_ev_mutex);
        int n_prompts = m_proc_ev / m_prompt_ev;
        m_proc_ev += to_update;
        int updated_prompt = proc_events() / m_prompt_ev;
        if (updated_prompt != n_prompts) {
            double t2 = m_watch.RealTime();
            std::cout << "Entry " << proc_events() << " / " << tot_events() << " (" << std::setprecision(2)
                      << (float)proc_events() / (float)tot_events() * 100. << "%)  @ " << LeptonTauMVA::TimeHMS(t2);
            std::cout << ". " << m_files_finished << "/" << num_files() << " have been finished.";
            std::cout << " Physics Event Rate: " << std::setprecision(3) << (m_proc_ev / t2 / 1000)
                      << " kHz, E.T.A.: " << LeptonTauMVA::TimeHMS(t2 * ((float)tot_events() / (float)proc_events() - 1.));
            std::cout << " (updating screen each " << m_prompt_ev << " events)";
            std::cout << std::endl;
            m_watch.Continue();
        }
    }
    void EventCounter::finish_file(Long64_t ev_in_file, Long64_t proc_events, Long64_t prompt_interval) {
        ev_in_file -= proc_events;
        ev_in_file += proc_events % prompt_interval;
        update_counter(ev_in_file);
        std::lock_guard<std::mutex> guard(m_proc_ev_mutex);
        ++m_files_finished;
    }

}  // namespace LeptonTauMVA
