#include <HDF5Writing/Utils.h>
#include <LeptonsInTauMVA/SampleGroup.h>
#include <LeptonsInTauMVA/Utils.h>

namespace LeptonTauMVA {
    SampleGroup::SampleGroup(const std::vector<int>& dsids, unsigned int limit_events, unsigned int ev_types) :
        m_dsids(dsids), m_ev_max(limit_events), m_ev_cached(0), m_types(ev_types), m_mutex_add_event(), m_mutex_increment() {
        std::sort(m_dsids.begin(), m_dsids.end());
    }

    void SampleGroup::add_sample(int dsid) {
        if (in_group(dsid)) return;
        m_dsids.push_back(dsid);
        std::sort(m_dsids.begin(), m_dsids.end());
    }
    unsigned int SampleGroup::events_filled() const { return m_ev_cached; }
    unsigned int SampleGroup::maximum() const { return m_ev_max; }

    bool SampleGroup::in_group(int dsid) const { return XAMPP::IsElementInList(m_dsids, dsid); }
    int SampleGroup::type_mask() const { return m_types; }

    bool SampleGroup::add_event(MuonAnalysis& t) {
        if (limit_reached() || !in_group(t.mcChannelNumber()) || !(type_mask() & (1 << t.classify_muon()))) { return false; }
        std::lock_guard<std::mutex> guard(m_mutex_add_event);
        if (!increment_counter()) return false;
        return true;
    }
    bool SampleGroup::increment_counter() {
        std::lock_guard<std::mutex> guard(m_mutex_increment);
        if (limit_reached()) return false;
        ++m_ev_cached;
        if (limit_reached()) {
            std::cout << "Sample " << m_types << " with DSIDs " << m_dsids << " is finished with " << m_ev_max << " events." << std::endl
                      << std::endl;
        }
        return true;
    }
    bool SampleGroup::limit_reached() const { return m_ev_cached >= m_ev_max; }
    const std::vector<int>& SampleGroup::dsids() const { return m_dsids; }

    //#########################################
    //              FlatlShapeGroup
    //#########################################
    FlatlShapeGroup::FlatlShapeGroup(const std::vector<int>& dsids, unsigned int limit_events, std::shared_ptr<TH1> histo_shape,
                                     unsigned int ev_types, std::function<int(MuonAnalysis& t, TH1*)> find_bin) :
        SampleGroup(dsids, limit_events, ev_types),
        m_histo(clone(histo_shape, true)),
        m_finder(find_bin),
        m_n_bins(GetNVisBins(histo_shape)),
        m_ev_per_bin(0),
        m_mutex_add_event() {
        m_ev_per_bin = (limit_events - limit_events % m_n_bins) / m_n_bins;
    }
    bool FlatlShapeGroup::add_event(MuonAnalysis& t) {
        if (limit_reached() || !in_group(t.mcChannelNumber()) || !(type_mask() & (1 << t.classify_muon()))) { return false; }

        int bin = m_finder(t, m_histo.get());
        /// Ignore the overflow bins
        if (isOverflowBin(m_histo, bin)) return false;

        std::lock_guard<std::mutex> guard(m_mutex_add_event);
        if (m_histo->GetBinContent(bin) >= m_ev_per_bin) {
            if (events_filled() + m_n_bins < maximum()) return false;
            if (maximum() % m_n_bins == 0) return false;
        }
        m_histo->SetBinContent(bin, m_histo->GetBinContent(bin) + 1);
        if (!increment_counter()) { return false; }
        return true;
    }
    //#########################################
    //              SampleGroupMgr
    //#########################################
    SampleGroupMgr::SampleGroupMgr() : m_samples() {}
    void SampleGroupMgr::add_sample(const std::vector<int>& dsids, unsigned int limit_events, unsigned ev_types) {
        add_sample(std::make_shared<SampleGroup>(dsids, limit_events, ev_types));
    }
    void SampleGroupMgr::add_sample(std::shared_ptr<SampleGroup> grp) {
        if (!grp) return;
        m_samples.push_back(grp);
    }
    bool SampleGroupMgr::add_event(MuonAnalysis& t) {
        for (auto& smp : m_samples) {
            if (smp->add_event(t)) { return true; }
        }
        return false;
    }
    bool SampleGroupMgr::empty() const {
        return m_samples.size() ==
               count<std::shared_ptr<SampleGroup>>(m_samples, [](const std::shared_ptr<SampleGroup>& grp) { return grp->limit_reached(); });
    }
    bool SampleGroupMgr::is_needed(unsigned int dsid) const {
        for (const auto& smp : m_samples) {
            if (!smp->limit_reached() && smp->in_group(dsid)) return true;
        }
        return false;
    }
}  // namespace LeptonTauMVA
