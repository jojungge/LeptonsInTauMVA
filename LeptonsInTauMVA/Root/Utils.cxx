#include <LeptonsInTauMVA/Utils.h>
#include <PathResolver/PathResolver.h>
#include <TH2Poly.h>
#include <dirent.h>

#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>

std::ostream &operator<<(std::ostream &os, TStopwatch &cpu_timer) {
    double t2 = cpu_timer.RealTime();
    os << std::setprecision(6) << " @ " << LeptonTauMVA::TimeHMS(t2);
    cpu_timer.Continue();
    return os;
}
namespace LeptonTauMVA {
    std::string to_string(PerigeeParams param) {
        if (param == PerigeeParams::d0) return "d0";
        if (param == PerigeeParams::z0) return "z0";
        if (param == PerigeeParams::phi0) return "phi0";
        if (param == PerigeeParams::theta) return "theta";
        if (param == PerigeeParams::qOverP) return "qOverP";

        return "UNKNOWN";
    }
    std::string to_string(PerigeeParams param1, PerigeeParams param2) {
        if (param1 > param2) return to_string(param2, param1);
        return to_string(param1) + "_" + to_string(param2);
    }
    std::vector<int> make_range(int start, int end) {
        if (start > end) return make_range(end, start);
        std::vector<int> to_ret;
        to_ret.reserve(end - start);
        for (int i = start; i <= end; ++i) to_ret.push_back(i);
        return to_ret;
    }
    std::vector<std::string> get_good_files(const std::vector<std::string> &in_files, const std::string &tree_name, bool skip_empty) {
        Long64_t tot_events = 0;
        return get_good_files(in_files, tree_name, tot_events, skip_empty);
    }
    std::vector<std::string> ReadList(const std::string &list_path) {
        std::vector<std::string> out;
        std::ifstream file_stream;
        file_stream.open(PlotUtils::GetFilePath(list_path));
        if (!file_stream.good()) return out;
        std::string line;
        while (GetLine(file_stream, line)) { out.push_back(line); }

        return out;
    }
    std::vector<std::string> get_good_files(const std::vector<std::string> &in_files, const std::string &tree_name, Long64_t &tot_events,
                                            bool skip_empty) {
        std::vector<std::string> good;
        typedef std::pair<Long64_t, std::string> good_tree;
        std::vector<good_tree> good_with_size;
        tot_events = 0;

        std::cout << "Check the input files" << std::endl;

        for (const auto &f_path : in_files) {
            std::cout << " Open: " << f_path << std::endl;
            std::shared_ptr<TFile> r_file = PlotUtils::Open(f_path);
            if (!r_file) continue;

            TTree *ana_tree = nullptr;
            r_file->GetObject(tree_name.c_str(), ana_tree);
            if (!ana_tree) {
                std::cerr << "Failed to load TTree " << tree_name << " from file " << f_path << std::endl;
                continue;
            }
            tot_events += ana_tree->GetEntries();
            if (!skip_empty || ana_tree->GetEntries()) {
                std::cout << " -- ok with " << ana_tree->GetEntries() << std::endl;
                good_tree entry(ana_tree->GetEntries(), f_path);
                if (!XAMPP::IsElementInList(good_with_size, entry)) good_with_size.push_back(entry);
            }
            delete ana_tree;
        }
        std::sort(good_with_size.begin(), good_with_size.end(), [](const good_tree &a, const good_tree &b) { return a.first > b.first; });
        good.reserve(good_with_size.size());
        for (auto &item : good_with_size) { good.emplace_back(item.second); }
        return good;
    }

    std::string LongestCommonString(const std::string &str1, const std::string &str2) {
        std::string common_string;
        if (str1.size() < str2.size()) return LongestCommonString(str2, str1);
        for (size_t l = 0; l <= str2.size(); ++l) {
            std::string sub_str = str2.substr(0, l);
            if (str1.find(sub_str) == 0) common_string = sub_str;
        }
        return common_string;
    }
    std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &FileList) {
        std::vector<std::string> ResolvedList;
        for (auto &File : FileList) {
            std::string F = PlotUtils::GetFilePath(File);
            if (!F.empty()) {
                std::cout << "GetPathResolvedFileList()  --- INFO:  Add file " << F << std::endl;
                ResolvedList.push_back(F);
            }
        }
        return ResolvedList;
    }

    std::string TimeHMS(float t) {
        std::stringstream ostr;
        ostr << std::setw(2) << std::setfill('0') << (int)((t / 60. / 60.)) % 24 << ":" << std::setw(2) << std::setfill('0')
             << ((int)(t / 60.)) % 60 << ":" << std::setw(2) << std::setfill('0') << ((int)t) % 60;
        return ostr.str();
    }
    bool GetLine(std::ifstream &inf, std::string &line) {
        if (!std::getline(inf, line)) return false;
        line = EraseWhiteSpaces(line);
        if (line.find("#") == 0 || line.size() < 1) return GetLine(inf, line);
        return true;
    }
    std::string EraseWhiteSpaces(std::string str) {
        str.erase(std::remove(str.begin(), str.end(), '\t'), str.end());
        while (!str.empty() && std::find_if(str.begin(), str.end(), [](unsigned char c) { return std::isspace(c); }) == str.begin())
            str = str.substr(1, std::string::npos);
        while (!str.empty() && std::find_if(str.rbegin(), str.rend(), [](unsigned char c) { return std::isspace(c); }) == str.rbegin())
            str = str.substr(0, str.size() - 1);
        return str;
    }
    std::vector<std::string> ListDirectory(std::string Path, const std::string &WildCard) {
        std::vector<std::string> List;
        DIR *dir;
        dirent *ent;
        std::cout << "ListDirectory() --- INFO: Read content of directory " << std::endl;
        if ((dir = opendir(Path.c_str())) != nullptr) {
            while ((ent = readdir(dir)) != nullptr) {
                std::string Entry = ent->d_name;
                if (Entry == "." || Entry == "..") continue;  // skip the .. and . lines in ls
                if (WildCard.size() == 0 || Entry.find(WildCard) < Entry.size()) List.push_back(Path + "/" + Entry);
            }
            closedir(dir);
        } else {
            std::cerr << "ListDirectory() --- ERROR: No such file or directory " << Path << std::endl;
        }
        return List;
    }
    bool DoesFileExist(const std::string &Path) { return std::ifstream(Path).is_open(); }
    void pull_overflow(std::shared_ptr<TH1> H) {
        std::function<int(TAxis *, int)> shift_bin = [](TAxis *A, int bin) {
            if (bin == 0) return 1;
            if (isOverflowBin(A, bin)) return bin - 1;
            return bin;
        };
        for (int b = GetNbins(H) - 1; b >= 0; --b) {
            if (!isOverflowBin(H, b)) continue;
            int x(0), y(0), z(0);
            H->GetBinXYZ(b, x, y, z);
            /// Determine the target X
            int t_x(shift_bin(H->GetXaxis(), x)), t_y(y), t_z(z);
            if (H->GetDimension() > 1) t_y = shift_bin(H->GetYaxis(), y);
            if (H->GetDimension() > 2) t_z = shift_bin(H->GetZaxis(), z);
            int target_bin = H->GetBin(t_x, t_y, t_z);
            H->SetBinContent(target_bin, H->GetBinContent(target_bin) + H->GetBinContent(b));
            H->SetBinError(target_bin, std::hypot(H->GetBinError(target_bin), H->GetBinError(b)));
            H->SetBinContent(b, 0);
            H->SetBinError(b, 0);
        }
    }
    unsigned int GetNbins(const std::shared_ptr<TH1> &Histo) { return GetNbins(Histo.get()); }
    unsigned int GetNbins(const TH1 *Histo) {
        if (!Histo) return 0;
        if (Histo->GetDimension() == 2) {
            const TH2Poly *poly = dynamic_cast<const TH2Poly *>(Histo);
            if (poly != nullptr) return poly->GetNumberOfBins() + 1;
        }
        unsigned int N = Histo->GetNbinsX() + 2;
        if (Histo->GetDimension() >= 2) N *= (Histo->GetNbinsY() + 2);
        if (Histo->GetDimension() == 3) N *= (Histo->GetNbinsZ() + 2);
        return N;
    }
    unsigned int GetNVisBins(const std::shared_ptr<TH1> &Histo) { return GetNVisBins(Histo.get()); }
    unsigned int GetNVisBins(const TH1 *Histo) {
        if (!Histo) return 0;
        unsigned int n_bins = Histo->GetNbinsX();
        if (Histo->GetDimension() >= 2) n_bins *= Histo->GetNbinsY();
        if (Histo->GetDimension() == 3) n_bins *= Histo->GetNbinsZ();
        return n_bins;
    }
    /// Returns the number of overlfow bins
    unsigned int GetNOverFlowBins(const std::shared_ptr<TH1> &Histo) { return GetNOverFlowBins(Histo.get()); }
    unsigned int GetNOverFlowBins(const TH1 *Histo) {
        if (!Histo) return 0;
        if (Histo->GetDimension() == 2) return 2;
        return GetNbins(Histo) - GetNVisBins(Histo);
    }
    bool isOverflowBin(const std::shared_ptr<TH1> &Histo, int bin) { return isOverflowBin(Histo.get(), bin); }
    bool isOverflowBin(const TH1 *Histo, int bin) {
        int x(-1), y(-1), z(-1);
        Histo->GetBinXYZ(bin, x, y, z);
        if (isOverflowBin(Histo->GetXaxis(), x)) return true;
        if (Histo->GetDimension() >= 2 && isOverflowBin(Histo->GetYaxis(), y)) return true;
        if (Histo->GetDimension() == 3 && isOverflowBin(Histo->GetZaxis(), z)) return true;
        return false;
    }
    bool isOverflowBin(const TAxis *a, int bin) { return bin <= 0 || bin >= a->GetNbins() + 1; }

    std::shared_ptr<TH1> MakeTH1(std::vector<double> BinEdgesX, std::vector<double> BinEdgesY, std::vector<double> BinEdgesZ) {
        ClearFromDuplicates(BinEdgesX);
        ClearFromDuplicates(BinEdgesY);
        ClearFromDuplicates(BinEdgesZ);
        std::sort(BinEdgesX.begin(), BinEdgesX.end());
        std::sort(BinEdgesY.begin(), BinEdgesY.end());
        std::sort(BinEdgesZ.begin(), BinEdgesZ.end());

        std::shared_ptr<TH1> H;
        if (!BinEdgesX.empty() && !BinEdgesY.empty() && !BinEdgesZ.empty()) {
            H = std::make_shared<TH3D>(PlotUtils::RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data(),
                                       BinEdgesY.size() - 1, BinEdgesY.data(), BinEdgesZ.size() - 1, BinEdgesZ.data());
        } else if (!BinEdgesX.empty() && !BinEdgesY.empty()) {
            H = std::make_shared<TH2D>(PlotUtils::RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data(),
                                       BinEdgesY.size() - 1, BinEdgesY.data());
        } else if (!BinEdgesX.empty()) {
            H = std::make_shared<TH1D>(PlotUtils::RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data());
        }
        if (H) H->SetDirectory(nullptr);
        return H;
    }
    std::shared_ptr<TH1> clone(const std::shared_ptr<TH1> &histo, bool reset) { return clone(histo.get(), reset); }
    std::shared_ptr<TH1> clone(const TH1 *histo, bool reset) {
        if (!histo) return std::shared_ptr<TH1>();
        std::shared_ptr<TH1> cloned(dynamic_cast<TH1 *>(histo->Clone(PlotUtils::RandomString(85).c_str())));
        if (reset) cloned->Reset();
        cloned->SetDirectory(0);
        return cloned;
    }
}  // namespace LeptonTauMVA
