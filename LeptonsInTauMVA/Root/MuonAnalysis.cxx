#include <FourMomUtils/xAODP4Helpers.h>
#include <LeptonsInTauMVA/MuonAnalysis.h>
#include <LeptonsInTauMVA/SampleGroup.h>
#include <LeptonsInTauMVA/Utils.h>

namespace LeptonTauMVA {

    MuonAnalysis::MuonAnalysis(TTree* t) : MuonTree_v5(t) {}
    /// The association of the IFF_type numbers to their physical meaning can be found here
    /// Prompt muons have  IFF_type == 4
    /// https://gitlab.cern.ch/ATLAS-IFF/IFFTruthClassifier/-/blob/master/IFFTruthClassifier/IFFTruthClassifierDefs.h#L13
    bool MuonAnalysis::is_prompt_muon() { return !Muons_fromTauDecay() && Muons_IFF_type() == 4; }
    bool MuonAnalysis::is_from_prompt_tau() { return Muons_fromTauDecay() && Muons_IFF_type() == 4; }
    bool MuonAnalysis::is_background_muon() { return Muons_IFF_type() != 4; }
    float MuonAnalysis::rho_prime() { return (Muons_calib_ptID() - Muons_calib_ptMS()) / Muons_pt(); }
    float MuonAnalysis::qover_p_significance() {
        /// determine the charges of the particular tracks
        int id_q = IDTracks_qOverP() > 0 ? 1 : -1;
        int ms_q = MSTracks_qOverP() > 0 ? 1 : -1;

        float sth = sin_theta();
        float q_over_p_id = sth * id_q / Muons_calib_ptID();
        float q_over_p_ms = sth * ms_q / Muons_calib_ptMS();
        return (q_over_p_id - q_over_p_ms) /
               std::sqrt(ID_cov(PerigeeParams::qOverP, PerigeeParams::qOverP) + MS_cov(PerigeeParams::qOverP, PerigeeParams::qOverP));
    }
    float MuonAnalysis::track_iso() { return Muons_ptvarcone30_TightTTVA_pt1000() / Muons_pt(); }
    float MuonAnalysis::calo_iso() { return Muons_topoetcone20() / Muons_pt(); }
    float MuonAnalysis::muon_vtx_z0() { return CBTracks_z0_primary() - CBTracks_vz(); }
    /// Use the identity sin(theta) = 1 / cosh(eta)
    float MuonAnalysis::sin_theta() { return 1. / std::cosh(Muons_eta()); }
    float MuonAnalysis::d0_significance() {
        return IDTracks_d0_significance();  // ? std::abs(CBTracks_d0_significance() / CBTracks_d0()) : 0;
    }
    float MuonAnalysis::z0_significance() { return IDTracks_z0_primary() / std::sqrt(ID_cov(PerigeeParams::z0, PerigeeParams::z0)); }
    float MuonAnalysis::rel_pt_err() { return IDTracks_pt_err() / IDTracks_pt(); }
    int MuonAnalysis::classify_muon() {
        if (is_prompt_muon()) {
            return LeptonTauMVA::SampleGroup::b_PromptMuon;
        } else if (is_from_prompt_tau()) {
            return LeptonTauMVA::SampleGroup::b_PromptMuonFromTau;
        }
        return LeptonTauMVA::SampleGroup::b_Background;
    }
    float MuonAnalysis::ID_chi2OverNDoF() { return IDTracks_chi2() / IDTracks_nDoF(); }
    float MuonAnalysis::MS_chi2OverNDoF() { return MSTracks_chi2() / MSTracks_nDoF(); }
    float MuonAnalysis::CB_chi2OverNDoF() { return CBTracks_chi2() / CBTracks_nDoF(); }

    ///
    /// v5 methods
    ///
    int MuonAnalysis::cov_idx(int i, int j) const {
        if (j > i) return cov_idx(j, i);
        return i * (i + 1) / 2 + j;
    }
    std::vector<double> MuonAnalysis::cov_to_vec(std::function<float(int i, int j)> func, const size_t s) {
        std::vector<double> vec;
        vec.reserve(s);
        for (int i = 0; i < PerigeeParams::num_perigess; ++i) {
            for (int j = 0; j <= i; ++j) { vec.push_back(func(i, j)); }
        }
        return vec;
    }

    float MuonAnalysis::sign_sqrt(const float x) const { return std::sqrt(std::abs(x)) * (x > 0 ? 1 : -1); }
    float MuonAnalysis::ID_cov(int i, int j) { return IDTracks_cov(cov_idx(i, j)); }
    float MuonAnalysis::CB_cov(int i, int j) { return CBTracks_cov(cov_idx(i, j)); }
    float MuonAnalysis::MS_cov(int i, int j) { return MSTracks_cov(cov_idx(i, j)); }
    float MuonAnalysis::sqrt_ID_cov(int i, int j) { return sign_sqrt(ID_cov(i, j)); }
    float MuonAnalysis::sqrt_CB_cov(int i, int j) { return sign_sqrt(CB_cov(i, j)); }
    float MuonAnalysis::sqrt_MS_cov(int i, int j) { return sign_sqrt(MS_cov(i, j)); }

    float MuonAnalysis::ID_param(int i) {
        if (i == PerigeeParams::d0) return IDTracks_d0();
        if (i == PerigeeParams::z0) return IDTracks_z0_primary();
        if (i == PerigeeParams::phi0) return IDTracks_phi();
        if (i == PerigeeParams::theta) return IDTracks_theta();
        if (i == PerigeeParams::qOverP) return IDTracks_qOverP();
        throw std::runtime_error(Form("Invalid perigee param %d", i));
        return FLT_MAX;
    }
    float MuonAnalysis::CB_param(int i) {
        if (i == PerigeeParams::d0) return CBTracks_d0();
        if (i == PerigeeParams::z0) return CBTracks_z0_primary();
        if (i == PerigeeParams::phi0) return CBTracks_phi();
        if (i == PerigeeParams::theta) return CBTracks_theta();
        if (i == PerigeeParams::qOverP) return CBTracks_qOverP();
        throw std::runtime_error(Form("Invalid perigee param %d", i));
        return FLT_MAX;
    }
    float MuonAnalysis::MS_param(int i) {
        if (i == PerigeeParams::d0) return MSTracks_d0();
        if (i == PerigeeParams::z0) return MSTracks_z0_primary();
        if (i == PerigeeParams::phi0) return MSTracks_phi();
        if (i == PerigeeParams::theta) return MSTracks_theta();
        if (i == PerigeeParams::qOverP) return MSTracks_qOverP();
        throw std::runtime_error(Form("Invalid perigee param %d", i));
        return FLT_MAX;
    }

    float MuonAnalysis::ID_perigee_sig(int i, int j) {
        double cov = ID_cov(i, j);
        if (cov == 0.) cov = FLT_MAX;
        double params = ID_param(i) * ID_param(j);
        return std::abs(params / cov);
    }
    float MuonAnalysis::MS_perigee_sig(int i, int j) {
        double cov = MS_cov(i, j);
        if (cov == 0.) cov = FLT_MAX;
        double params = MS_param(i) * ID_param(j);
        return std::abs(params / cov);
    }
    float MuonAnalysis::CB_perigee_sig(int i, int j) {
        double cov = CB_cov(i, j);
        if (cov == 0.) cov = FLT_MAX;
        double params = CB_param(i) * CB_param(j);
        return std::abs(params / cov);
    }
    std::vector<double> MuonAnalysis::ID_cov_vec() {
        return cov_to_vec([this](int i, int j) { return ID_cov(i, j); });
    }
    std::vector<double> MuonAnalysis::CB_cov_vec() {
        return cov_to_vec([this](int i, int j) { return CB_cov(i, j); });
    }
    std::vector<double> MuonAnalysis::MS_cov_vec() {
        return cov_to_vec([this](int i, int j) { return MS_cov(i, j); });
    }
    std::vector<double> MuonAnalysis::sqrt_ID_cov_vec() {
        return cov_to_vec([this](int i, int j) { return sqrt_ID_cov(i, j); });
    }
    std::vector<double> MuonAnalysis::sqrt_CB_cov_vec() {
        return cov_to_vec([this](int i, int j) { return sqrt_CB_cov(i, j); });
    }
    std::vector<double> MuonAnalysis::sqrt_MS_cov_vec() {
        return cov_to_vec([this](int i, int j) { return sqrt_MS_cov(i, j); });
    }
    std::vector<double> MuonAnalysis::ID_perigee_sig_vec() {
        return cov_to_vec([this](int i, int j) { return ID_perigee_sig(i, j); });
    }
    std::vector<double> MuonAnalysis::MS_perigee_sig_vec() {
        return cov_to_vec([this](int i, int j) { return MS_perigee_sig(i, j); });
    }
    std::vector<double> MuonAnalysis::CB_perigee_sig_vec() {
        return cov_to_vec([this](int i, int j) { return CB_perigee_sig(i, j); });
    }

}  // namespace LeptonTauMVA
