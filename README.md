# LeptonsInTauMVA
This project is further processes the files created by the [LeptonsInTaus](https://gitlab.cern.ch/jojungge/leptonsintaus) framework in 
a format usable by the common machine learning frameworks like [SciKitLearn](https://scikit-learn.org),  or [Keras](https://keras.io/). 
The ROOT files are transformed into HDF5 files using the [HDF5Writing](https://gitlab.cern.ch/atlas-mpp-xampp/hdf5writing) library. 
This library allows for an easy split up of the labeleled data into `Training`  and `Testing` in a consistent manner such that [h5py](https://www.h5py.org/) can be used to pipe the data to the MVA classifier of interest.


How to clone the repository
---------------------------
The code is mainly based on the  [NtupleAnalysisUtils](https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils) framework developed by @goblirsc to easily and efficiently process ROOT files in any given format.
In order to clone the repository generate a private ssh key on your machine, e.g. `lxplus` or `mppui4.t2.rzg.mpg.de` and make sure that it is added to your [GitLab keys](https://gitlab.cern.ch/profile/keys). Then execute the following commands
```
mkdir MyTestArea
cd MyTestArea
git clone --recursive ssh://git@gitlab.cern.ch:7999/jojungge/LeptonsInTauMVA.git source
```
This command will download the primary code and all other packages on which the code depends. The extra `source` argument renames  the cloned repository locally, which is just a common style convention in most cases. 
Alternatively you can also use `Kerberos` authentification method for cloning your repository, but make then sure to exchange the `GitLab` URL properly. 
To my taste `Kerberos` is less convenient as you have always to type in your password if you want to commit changes to the repository.

How to compile
--------------
As any code in `ATLAS` the compilation of the code is based on the `CMake` build system working with the `ATLAS` software. 
For the compilation of the code please first login to a node having access to the ATLAS software, e.g. `lxplus` or `mppui4.t2.rzg.mpg.de` and execute the following sequence of commands 
```
cd MyTestArea/
mkdir build/
asetup AthAnalysis,21.2.112,here
cd build/
cmake ../source/
make -j 64
source ../build/x86_64-centos7-gcc62-opt/setup.sh
```
The first two lines are changing to the area in which you've cloned the code from git and creating the `build` directory. 
The latter is used to configure the compilation and finally to create the binaries of your compiled programme. 
The fourth line loads the `ATLAS` software into the environment of your computer, i.e. it makes the code available for execution. 
There are many flavours of the `ATLAS` software depending on the purpose each built from the central [athena](https://gitlab.cern.ch/atlas/athena) code repository. 
The flavours differ in the features available. 
For instance, `Athena` contains components to load the detector geometry or magnetic fields and also to access each particular detector modul in order to reconstruct the full event. 
But is rather heavy and hence slow. In contrast, `AthAnalysis` only has everything required for data analysis purposes. 
The `AthDerivation` releases are used to run the [DerivationFramework](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DerivationFramework). 
This framework creates skimmed copies from the primary reconstructed data and also fixes bugs made in the reconstruction software.
The `ATLAS` software is not a static construct once written and then sitting there static for all time but it has been under static development. 
New analysis techniques, object calibrations are added or bugs in the code are removed in each new release encoded in the sequence of numbers. 
The last argument saves a snippet of the setup you've used into the local folder.
In the firth line the `CMake` build system is configured to compile the software against the libraries and tools provided by the version of the ATLAS software in use and the next actually compiles your code. 
Everytime you have changed something in your `C++` code, you must come back to the build folder and compile the changes via `make`. 
If you have added new source files, then you'll have to call `cmake` as well.
Finally, the computer has to know where to find the freshly compiled libraries and tools which is configured in the last line.

Everytime you login again to the computing centre, you will start with a fresh environment knowing nothing about what you've done in previous sessions. 
It is quite useful as it allows you to simultaenously work on different projects without reinstalling the computer from scratch. In order to reload the environment from the last session please do:
```
cd MyTestArea
asetup --restore
cd source
source ../build/x86_64-centos7-gcc62-opt/setup.sh
```

How to run
----------
Cuurently there are two executable available in the repository. `MakeMuonFeaturePlots` and `MakeMuonHDF5`. The former uses the sophisitaced `NtupleAnalysisUtils` to create nice variable distributions for muons from prompt decays, from prompt decays where the decaying particle decayed first into a leptonically decaying tau --- that's the case in which we are interested -- and finally for muons from secondary decays in a single plot. The latter writes the `HDF5` file which can be then parsed to the machine learning. Be aware that the features implemented are just for illustration purposes and do not neccessarily represent any physical meaning. Have a look a the source code of each exectuable which you find in the [util](https://gitlab.cern.ch/jojungge/LeptonsInTauMVA/-/tree/feature_add/LeptonsInTauMVA/util) directory. The most simple way to use the `MakeMuonFeaturePlots` programme is via a text file containing the paths to all of your input data. Each line represents one input file which are all read by the programme sequentially. To launch the tool just type:
```
export NTAU_NTHREADS=6
MakeMuonFeaturePlots --fileList LeptonsInTauMVA/data/fileLists.txt --outDir tortoises/
```
The first line limits the number of parallel threads to `6` which is a compromise between being fast readout and not annoying other users of the system. Actually, it only needs to be executed once per session, if you like you can consider it as part of your initialization ritual. The other one is actually executing the programme which is saving the final plots in the `tortoises` directory. The programme has also the `--inFile` where you parse each file separately to the tool and the `--dir` option where all `root` files in the given directory are considered for the analysis. The `MakeMuonHDF5` tool runs in an analogous manner, but with the difference that it has the `--outFile` argument instead of the `--outDir` argument to change the output name of the file.


